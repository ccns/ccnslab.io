import sys
sys.path.append('.')

from baseconf import *  # noqa

RELATIVE_URLS = True
SITEURL = ''
PAGE_URL = '{path_no_ext}/index.html'
