from pathlib import Path


def is_group_with_logo(group):
    logo_file = Path('themes/ccns/static/images/logo_' + group + '.png')
    print(logo_file)
    if logo_file.is_file():
        return True
    return False
