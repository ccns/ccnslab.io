from plugins.pure import research_output_to_dict
import pickle
from pathlib import Path
import os

if __name__ == '__main__':
    pure_api_key = os.getenv('PURE_API_KEY')
    if not pure_api_key:
        raise RuntimeError('Please set the PURE_API_KEY environment variable')
    pure_data = research_output_to_dict(pure_api_key)

    with open(Path('misc_data', 'pure_data.dat'), 'wb') as pure_file:
        pickle.dump(pure_data, pure_file)
