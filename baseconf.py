import sys
sys.path.append('.')

import jinjafilters  # noqa
import jinjatests  # noqa
import jinja_extensions  # noqa

AUTHOR = 'Mateusz Pawlik'
SITENAME = 'CCNS - Centre for Cognitive Neuroscience'

PATH = 'content'

STATIC_PATHS = ['images', 'assets/cv']

TIMEZONE = 'Europe/Rome'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = False

# Plugins
PLUGIN_PATHS = ['plugins']
PLUGINS = ['ccns-hierarchy', 'pure', 'jinjainmd', 'ccns_members']

# GitLab Pages Review App:
# https://gitlab.com/gitlab-org/gitlab/-/issues/16907#note_254753037
# requires now urls ending with `index.html`

# Keep looking at https://gitlab.com/gitlab-org/gitlab/-/issues/16907
# and https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71995

# The first group is to remove `pages` part.
PATH_METADATA = '(pages/)(?P<path_no_ext>.*)\..*'  # noqa
# Used for handling `index.html` suffix, otherwise False - both work.
PAGE_SAVE_AS = '{path_no_ext}/index.html'

# Set article urls under their categories
ARTICLE_URL = '{category}/{slug}.html'
ARTICLE_SAVE_AS = '{category}/{slug}.html'

# Limit rendered content
TAG_SAVE_AS = ''
TAGS_SAVE_AS = ''
CATEGORY_SAVE_AS = ''
CATEGORIES_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
AUTHORS_SAVE_AS = ''
ARCHIVES_SAVE_AS = ''

# Jinja2 custom filters
JINJA_FILTERS = {
    'filter_articles': jinjafilters.filter_articles
}

# Jinja2 custom tests
JINJA_TESTS = {
    'group_with_logo': jinjatests.is_group_with_logo
}

JINJA_ENVIRONMENT = {
    'trim_blocks': True,
    'lstrip_blocks': True,
    'extensions': ['jinja2.ext.do'] + jinja_extensions.get_extensions()
}

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.abbr': {},
        'markdown.extensions.def_list': {},
        'markdown.extensions.fenced_code': {},
        'markdown.extensions.footnotes': {},
        'markdown.extensions.tables': {},
        'markdown.extensions.md_in_html': {},
        'markdown.extensions.meta': {},
        'markdown_extensions.jinja_in_markdown': {}
    },
    'output_format': 'html5',
}
