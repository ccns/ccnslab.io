---
Title: Sleep, Cognition and Consciousness
Summary: This research cluster explores the interplay between consciousness and cognition. On the one hand, we study how information processing changes in states of altered consciousness such as natural sleep, and on the other hand we focus on cognitive processing in newborns and young infants. The choice of the method given the study topic thereby is usually (high-density) EEG and/or MEG.
Responsible: Manuel
Image: images/clusters/clusters_sleep.jpg
Order: 7
---

The research cluster “sleep, cognition, consciousness“ explores the interesting interplay between consciousness and cognition. On the one hand, we study how information processing changes in states of altered consciousness such as natural occurring sleep. We hereby, follow-up on recent evidence that the sleeping brain can even distinguish complex auditory information (such as voice familiarity) in complete absence of consciousness such as  deep (N3) NREM sleep or while dreaming (REM). Another topic drawing more and more attention is cognitive processing in newborns and young infants. Here we ask what the baby can already learn before birth and how we can demonstrate such learning just after birth using methods such as (high-density) EEG, MEG or heart-rate (ECG) responses. In addition we are here interested how stress and psychological strain on the side of the mother affects the child and its cognitive learning potential. Last but not least we study memory consolidation during sleep, that is the question how the brain re-structures and consolidates memories in an apparent - but obviously - misleading “offline” mode of brain processing.

On the more applied side, the cluster is also interested in how to improve sleep in order to have these vital functions be realized across society. Currently we here for example to research on so-called "insomniacs" (with sleep onset or maintenance problems) and develop non-pharmacological alternatives such as digital health solutions (see www.nukkuaa.com). For children and adolescents we ask questions such as “How SMART it is to go to bed with a phone” and measure sleep and memory changes due to the utilization of such devices just before sleep.
 
