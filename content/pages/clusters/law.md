---
Title: Neuroscience and the Law
Summary: This research cluster addresses, from a legal perspective, the intersection between current initiatives on Open Science, Open Data and the FAIR principles on the one hand, and the issue of data protection on the other. Questions about the privileged status of and public interest in neuroscience data will be explored. This will further our understanding of the legal basis for sharing and reanalysis of neuroscience data.
Responsible: Florian
Image: images/clusters/clusters_law.jpg
Order: 6
---

coming soon...
