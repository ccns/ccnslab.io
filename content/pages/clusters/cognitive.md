---
Title: Cognitive Neuroscience and Brain Disorders
Summary: This research cluster investigates the implementation of cognitive processes and their modulation by brain disorders. For instance, we investigate commonalities across disorders, viewing them along dimensions such as brain structure, function, and enocrinology, rather than distinct categories. Our approaches include characterizing the brain's connectivity patterns as a defining measure of brain function beyond topographical aspects. With such approaches, we aim to develop precise biomarkers for clinical diagnosis, treatment planning, and patient monitoring.
Responsible: Martin
Image: images/clusters/clusters_cogneuro.jpg
Order: 2
---

coming soon...
