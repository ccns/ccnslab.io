---
Title: Hormones and the Brain
Summary: Hormones regulate a variety of psychological functions, including our response to stress, social interactions, attachment, sleep and reproduction. The cluster is interested in the neurobiological underpinnings and neural correlates of these effects.
Responsible: Belinda
Image: images/clusters/clusters_hormones.png
Order: 5
---

Endocrinological processes are part of our body’s information processing system. Hormones regulate a variety of psychological functions, including our response to stress, social interactions, attachment, sleep and reproduction. The cluster “Hormones and the Brain” is interested in the neurobiological underpinnings and neural correlates of these effects. Which brain parameters respond to hormonal changes and what are the behavioural consequences? 
The work of this cluster spans the most important hormone systems of our body:

-	Stress hormones (Cortisol)
-	Sex hormones (Estradiol, Progesterone, Testosterone)
-	Sleep hormones (Melatonine)
-	“Social” hormones (Oxytocin)


