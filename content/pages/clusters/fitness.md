---
Title: Mental and Physical Fitness
Summary: By investigating the sporting brain and especially that of elite athletes, this research cluster strives for understanding the behaviour relevant to the normal population focusing on cognitive demands.
Responsible: Kerstin
Image: images/clusters/clusters_fitness.jpg
Order: 4
---

Within this research cluster we want to further strengthen the cooperation between researchers of the Department of Sport & Exercise Science and the CCNS which is already well-established. Researchers from both domains successfully obtained joint third-party funded projects on the neural correlates of sporting performance. Two leading research goals are the following (1) improving elite sporting performances and (2) studying sports elites to inform us about normal behavior. Several projects investigate brain and physiological reactivity during motor performance using EEG, fMRI and electrocardiography. Currently we strive for investigating the following research topics:

- Neural correlates of football expertise (Birklbauer, Richlan, Amesberger, Hödlmoser)

- Impact of heading a football on the brain (Birklbauer, Richlan, Amesberger, Trinka)

– Sleep and chronotypes in athletes (Hödlmoser, Birklbauer)

– Impact of sleep on (real-life) motor learning/adaptation (Hödlmoser, Birklbauer, Müller)

– Predicting and improving healthy eating and physical activity (Blechert)

– Kinematic investigation of articulator motion in Austrian Sign Language (Röhm, Schwameder)

– Sports-/Mental performance enhancement through Virtual Reality training (Richlan, Heed, Amesberger) 
