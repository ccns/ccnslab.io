---
Title: Developmental Aspects of Cognition
Summary: By studying infants and children from pre-natal stages until the end of adolesence, this cluster investigates the course of normal and atypical development, as well as its influencing factors. 
Responsible: Boukje
Image: images/clusters/clusters_development.jpg
Order: 3
---

coming soon...
