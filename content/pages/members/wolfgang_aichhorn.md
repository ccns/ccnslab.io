---
Title: Wolfgang Aichhorn
Date: 20.06.2022
Name: Wolfgang
Surname: Aichhorn
Position: Full Professor
Tags: ccns
Email: psychiatrie@salk.at
Phone: +43572553
Template: person
Status: hidden
Affiliations:
    Christian Doppler University Hospital - Paracelsus Medical University
    Department of Psychiatry, Psychotherapy and Psychosomatics
---
