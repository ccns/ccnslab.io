---
Title: Jürgen Birklbauer
Date: 20.06.2022
Name: Jürgen
Surname: Birklbauer
Position: Senior Scientist
Tags: ccns
Email: juergen.birklbauer@plus.ac.at
Status: hidden
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Sports Science and Kinesiology
---
