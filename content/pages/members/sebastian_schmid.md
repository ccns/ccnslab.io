---
Title: Sebastian Schmid
Date: 20.06.2022
Name: Sebastian
Surname: Schmid
Position: Full Professor
Tags: ccns
Email: sebastian.schmid@plus.ac.at
Phone: +4366280443623
Status: hidden
Affiliations:
    PLUS Faculty of Law
    Department of Public Law
---
