---
Title: Josef Perner
Date: 20.06.2022
Name: Josef
Surname: Perner
Position: Professor emeritus
Tags: ccns
Email: josef.perner@plus.ac.at
Phone: ' +4366280445124'
Photo: josef_perner.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Pure: 2585583
---