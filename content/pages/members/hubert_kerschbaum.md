---
Title: Hubert Kerschbaum
Date: 20.06.2022
Name: Hubert
Surname: Kerschbaum
Position: Associate Professor
Tags: ccns
Email: Hubert.Kerschbaum@plus.ac.at
Phone: +4366280445667 / 5681
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Biosciences
---
