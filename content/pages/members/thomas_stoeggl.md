---
Title: Thomas Stöggl
Date: 20.06.2022
Name: Thomas
Surname: Stöggl
Position: Full Professor
Tags: ccns
Email: thomas.stoeggl@plus.ac.at
Phone: +4366280444884
Status: hidden
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Sports Science and Kinesiology
---
