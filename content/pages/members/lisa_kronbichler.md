---
Title: Lisa Kronbichler
Date: 20.06.2022
Name: Lisa
Surname: Kronbichler
Position: Senior Scientist
Tags: ccns
Email: lisa.kronbichler@plus.ac.at
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---
