---
Title: Eugen Trinka
Date: 20.06.2022
Name: Eugen
Surname: Trinka
Position: Full Professor
Tags: ccns
Email: e.trinka@salk.at
Photo: eugen_trinka.jpg
Affiliations:
    Christian Doppler University Hospital - Paracelsus Medical University
    Department of Neurology, Neurointensive care & Neurorehabilitation
    Neuroscience Institute
---
