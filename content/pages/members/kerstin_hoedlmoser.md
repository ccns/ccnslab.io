---
Title: Kerstin Hödlmoser
Date: 20.06.2022
Name: Kerstin
Surname: Hödlmoser
Position: Associate Professor
Tags: ccns
Email: kerstin.hoedlmoser@plus.ac.at
Phone: ' +4366280445143'
Photo: kerstin_hoedlmoser.jpg
Pure: 2582074
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---

<b>Complete list of publications and further research output can be found [here](https://uni-salzburg.elsevierpure.com/en/persons/kerstin-h%C3%B6dlmoser-5)</b>

Current publications can be found below
