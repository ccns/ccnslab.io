---
Title: Andreas Kletecka
Date: 20.06.2022
Name: Andreas
Surname: Kletecka
Position: Full Professor
Tags: ccns
Email: Andreas.Kletecka@plus.ac.at
Phone: +4366280443301
Status: hidden
Affiliations:
    PLUS Faculty of Law
    Department of Private Law
---
