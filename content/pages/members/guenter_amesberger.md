---
Title: Günter Amesberger
Date: 20.06.2022
Name: Günter
Surname: Amesberger
Position: Full Professor
Tags: ccns
Email: Guenter.Amesberger@plus.ac.at
Phone: +43 6628044744857
Status: hidden
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Sports Science and Kinesiology
---
