---
Title: Thomas Finkenzeller
Date: 20.06.2022
Name: Thomas
Surname: Finkenzeller
Position: Associate Professor
Tags: ccns
Email: thomas.finkenzeller@plus.ac.at
Phone: +4366280444876 /4972
Status: hidden
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Sports Science and Kinesiology
---
