---
Title: Christina Plamberger
Date: 20.06.2022
Name: Christina
Surname: Plamberger
Position: PostDoc
Tags: ccns
Email: christina.plamberger@plus.ac.at
Phone: +4366280445137
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---
