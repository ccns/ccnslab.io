---
Title: Walter Gruber
Date: 20.06.2022
Name: Walter
Surname: Gruber
Position: Senior Lecturer
Tags: ccns
Email: walter.r.gruber@plus.ac.at
Phone: +4366280445115
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---
