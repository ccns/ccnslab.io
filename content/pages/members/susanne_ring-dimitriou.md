---
Title: Susanne Ring-Dimitriou
Date: 20.06.2022
Name: Susanne
Surname: Ring-Dimitriou
Position: Full Professor
Tags: ccns
Email: susanne.ring@plus.ac.at
Phone: +4366280444890
Status: hidden
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Sports Science and Kinesiology
---
