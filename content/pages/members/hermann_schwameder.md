---
Title: Hermann Schwameder
Date: 20.06.2022
Name: Hermann
Surname: Schwameder
Position: Full Professor
Tags: ccns
Email: Hermann.Schwameder@plus.ac.at
Phone: +4366280444859
Status: hidden
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Sports Science and Kinesiology
---
