---
Title: Belinda Plattner
Date: 20.06.2022
Name: Belinda
Surname: Plattner
Position: Full Professor
Tags: ccns
Email: b.plattner@salk.at
Phone: +435725534200
Status: hidden
Affiliations:
    Christian Doppler University Hospital - Paracelsus Medical University
    Department of Child and Adolescent Psychiatry
---
