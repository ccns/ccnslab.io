---
Title: Manuel Schabus
Date: 20.06.2022
Name: Manuel
Surname: Schabus
Position: Full Professor
Tags: ccns
Email: manuel.schabus@plus.ac.at
Phone: ' +4366280445113'
Photo: manuel_schabus.jpg
Pure: 2591774
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
CV: Schabus_Manuel.pdf
---

<b>Complete list of publications and further research output can be found [here](https://uni-salzburg.elsevierpure.com/en/persons/manuel-schabus-4)</b>

Current publications can be found below
