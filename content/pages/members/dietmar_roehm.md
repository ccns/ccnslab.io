---
Title: Dietmar Röhm
Date: 20.06.2022
Name: Dietmar
Surname: Röhm
Position: Full Professor
Tags: ccns
Email: dietmar.roehm@plus.ac.at
Phone: +4366280444271
Photo: dietmar_roehm.jpg
Affiliations:
    PLUS Faculty of Cultural and Social Sciences
    Department of Linguistics
---
