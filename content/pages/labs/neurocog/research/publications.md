---
Title: Publications
Order: 2
---

{% from 'publications.html' import publication_list with context %}

{{ publication_list('neurocog') }}