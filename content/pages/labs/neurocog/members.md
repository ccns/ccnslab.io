---
Title: Team
Order: 3
---

{% from 'members.html' import members with context %}

{{ members(positions=[['Full Professor'], ['Associate Professor'], ['Administrative Assistant'], ['PostDoc'], ['Senior Scientist'], ['PhD Student'], ['TA', 'BTA', 'RTA']]) }}
