---
Title: Stefan Hawelka
Date: 20.06.2022
Name: Stefan
Surname: Hawelka
Position: Senior Scientist
Tags: ccns, neurokog
Email: Stefan.Hawelka@plus.ac.at
Phone: ' +4366280445151'
Photo: stefan_hawelka.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Pure: 2593828
---