---
Title: Martin Kronbichler
Date: 20.06.2022
Name: Martin
Surname: Kronbichler
Position: Associate Professor
Tags: ccns, neurokog
Email: martin.kronbichler@plus.ac.at
Photo: martin_kronbichler.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
    Christian Doppler University Hospital     Paracelsus Medical University
    Neuroscience Institute
Pure: 2588331
---