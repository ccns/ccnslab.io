---
Title: Brigitte Koch-Stockinger
Date: 07.10.2022
Name: Brigitte
Surname: Koch-Stockinger
Position: Administrative Assistant
Tags: ccns, neurokog
Phone: +4366280445174
Email: brigitte.koch-stockinger@plus.ac.at
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---

