---
Title: Sebastian Krempelmeier
Date: 07.10.2022
Name: Sebastian
Surname: Krempelmeier
Position: Senior Scientist
Tags: ccns, neurokog
Email: Sebastian.Krempelmeier@plus.ac.at
Phone:  +4366280445170
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---

