---
Title: Nicole Himmelstoß
Date: 20.06.2022
Name: Nicole
Surname: Himmelstoß
Position: Senior Scientist
Tags: ccns, neurokog
Email: nicolealexandra.himmelstoss@plus.ac.at
Phone: ' +4366280445170'
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Pure: 4733062
---