---
Title: Florian Hutzler
Date: 20.06.2022
Name: Florian
Surname: Hutzler
Position: Full Professor
Tags: ccns, neurokog
Email: florian.hutzler@plus.ac.at
Phone: ' +4366280445114'
Photo: florian_hutzler.jpg
CV: Hutzler_Florian.pdf
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Pure: 2584683
---

# Research interests
I study normal and impaired visual word recognition using neurocognitive techniques such as functional magnetic resonance imaging, electroencephalography, eye movement recording, and computational modeling. I am interested in ecologically valid paradigms that expand the range of experimental settings in which the neural correlates of cognitive processes can be recorded. My research group focuses on coregistration of eye movements and EEG and eye movements and fMRI. In addition, I am fascinated by human fallacies such as the [reverse inference fallacy](https://www.sciencedirect.com/science/article/pii/S1053811913000141) or the [exponential growth bias](https://royalsocietypublishing.org/doi/full/10.1098/rsos.201574). Recently, we have been working on ontology-based knowledge systems that allow objective classification of neurocognitive datasets (check out [Austrian NeuroCloud](https://ccns.plus.ac.at/groups/neurocog/research/projects/)).

# Positions
* since 2014: Coordinator of the Centre for Cognitive Neuroscience
* since 2010: Professor of Developmental Psychology and Neurocognition
* 2008-2009: Visiting Professor, LMU Munich
* 2008: Invited visiting researcher, Macquarie Center for Cognitive Science, Sydney, Australia
* 2005-2007: Postdoc at the University of Vienna
* 2003-2005: Postdoc at the Free University of Berlin

# Administration
* Office Hours: please request appointment via e-mail to [Brigitte Koch-Stockinger](mailto:brigitte.koch-stockinger@plus.ac.at)
* my work on [ORCID](https://orcid.org/0000-0001-8195-4911)