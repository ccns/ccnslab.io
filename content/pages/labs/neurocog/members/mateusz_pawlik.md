---
Title: Mateusz Pawlik
Date: 20.06.2022
Name: Mateusz
Surname: Pawlik
Position: Senior Scientist
Tags: ccns, neurokog
Email: Mateusz.Pawlik@plus.ac.at
Phone: ' +4366280446356'
Photo: mateusz_pawlik.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Pure:
  id: 2581666
  types:
      article
      conference
  size: 100
---

My research journey started during my Erasmus 2006/2007 when I visited Free University of Bozen-Bolzano, Italy. I did an internship under the supervision of Prof. Johann Gamper and worked on the temporal multidimensional aggregation. In 2009 I received my M.Sc. degree in Computer Science from Adam Mickiewicz University of Poznań, Poland. I wrote my master thesis "Storing and Retrieving Trajectories of Mobile Objects in Spatio-Temporal Databases" under the supervision of Prof. Tadeusz Pankowski.

I went back to Free University of Bozen-Bolzano to do my PhD under the supervision of Prof. Nikolaus Augsten. I graduated in 2014 with the thesis "Efficient Computation of the Tree Edit Distance". The examination committee was composed of Prof. Alfons Kemper (Technische Universität München, Germany), Hélène Touzet (INRIA and University Lille, France), and Prof. Enrico Franconi (Free University of Bozen-Bolzano, Italy).

## Research Interests

My current research is focused on similarity of hierarchical data with an emphasis on ordered trees. I am devoted to developing efficient and highly applicable solutions for comparing tree structures in the context of tree-to-tree matching and similarity join queries. I am particularly interested in scalable algorithms for computing minimal and meaningful edit mappings between trees.

## Projects

Tree Edit Distance - reference website to measuring similarity of tree structured data using the tree edit distance measure. The runnable and source code of the RTED algorithm - most efficient tree edit distance algorithm.

## Teaching

### University of Salzburg

- Datenbanken Vertiefung. Labs. Winter Semester 2016/2017.
- Datenbanken 2. Labs. Winter Semester 2016/2017.
- Datenbanken 1. Labs. Summer Semester 2015/2016.
- Datenbanken Vertiefung. Labs. Winter Semester 2015/2016.
- Datenbanken 2. Labs. Winter Semester 2015/2016.
- Datenbanken 1. Labs. Summer Semester 2014/2015.
- Datenbanken Vertiefung. Labs. Winter Semester 2014/2015.
- Datenbanken 2. Labs. Winter Semester 2014/2015.

### Free University of Bozen-Bolzano

- Data Warehousing and Data Mining. Labs. Winter Semester 2012/2013.
- Data Warehousing and Data Mining. Labs. Winter Semester 2011/2012.