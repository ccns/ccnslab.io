---
Title: Fabio Richlan
Date: 20.06.2022
Name: Fabio
Surname: Richlan
Position: Senior Scientist
Tags: ccns, neurokog
Email: Fabio.Richlan@plus.ac.at
Photo: fabio_richlan.jpg
Phone: '  +4366280445130'
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Pure: 2584726
---

# Short biography and research interests
Fabio Richlan is a certified sport psychologist, research scientist, and lecturer at the Centre for Cognitive Neuroscience and the Department of Psychology at the Paris Lodron University of Salzburg in Austria. He is the co-leader of the research cluster “Mental and Physical Fitness”, where he investigates topics at the intersection between neuroscience and sport, exercise, and performance psychology.

He is also the founder and owner of an international business company for applied sport psychology ([MENTAL PERFORMANCE PRO](https://mental-performance-pro.com/)). For over a decade, he has been working with the most important sports organizations and has been coaching Austrian and international top-level athletes. In a ranking carried out at Stanford University, he is counted among the two percent of the world's most important scientists.

# Positions
* since 2014: Senior Scientist and co-leader of the research cluster "Mental & Physical Fitness", Centre for Cognitive Neuroscience, Paris-Lodron-University of Salzburg
* since 2014: Certified sport psychologist, founder and owner of a business company for applied sport psychology
* 2023-2024: Visiting Scholar, Basque Center on Cognition, Brain and Language, Donostia-San Sebastián, Spain
* 2017-2017: Visiting Scholar, Laboratory of Brain Imaging, Neurobiology Centre, Nencki Institute of Experimental Biology, Polish Academy of Sciences, Warsaw, Poland
* 2010-2014: Psychological technical assistant, Centre for Neurocognitive Research and Department of Psychology, University of Salzburg
* 2010-2010: Psychologist, NeuroCare Rehabilitation Clinic, Christian-Doppler-Clinic, Paracelsus Medical University, Salzburg
* 2009-2010: Psychologist, Institute for Neuroscience and Clinical Research Center Salzburg, Christian-Doppler-Clinic, Paracelsus Medical University, Salzburg

# Administration
* Fabio Richlan on [ORCID](https://orcid.org/0000-0001-5373-3425)
* Fabio Richlan on [Google Scholar](https://scholar.google.com/citations?user=3-9pMVUAAAAJ)