---
Title: Bianca Löhnert
Date: 07.10.2022
Name: Bianca
Surname: Löhnert
Position: PhD Student
Tags: ccns, neurokog
Email: bianca.loehnert@plus.ac.at
Phone: +4366280445170
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---
