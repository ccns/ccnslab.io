---
Title: Barbara Strasser-Kirchweger
Date: 07.10.2022
Name: Barbara
Surname: Strasser-Kirchweger
Position: PhD Student
Tags: ccns, neurokog
Email: barbara.strasser-kirchweger@plus.ac.at
Phone: +4366280445166
Photo: barbara_strasser.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---

