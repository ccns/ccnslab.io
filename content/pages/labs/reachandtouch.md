---
Title: Reach & Touch Lab
subtitle: 
summary: The Reach & Touch Lab investigates the interaction and integration of touch and movement.
Order: 4
---

{% import 'news.html' as news with context %}

![]({static}/images/labs/reachandtouch/landingpage/Team3.jpg)


We are the Cognitive Psychology Group at the Department of Psychology of the University of Salzburg. We're also part of the University's Centre for Cognitive Neuroscience.
We're responsible for teaching General Psychology (in German: Allgemeine Psychologie) in the BSc and MSc psychology study programs.
Our research focuses on movement - often arm movement - and touch. Therefore, our lab is the Reach & Touch Lab.
<br>
<br>
{% image_with_text "images/labs/reachandtouch/landingpage/motion_tracking.jpg", "Image: Simon Haigermoser", "left", "Our Research" %}
We ask how humans integrate touch and movement, and how these seemingly simple and basic sensorimotor processes contribute to the way we perceive and represent our bodies and ourselves.
To get a broad impression of our work, check out our review papers listed at the very top of [our Lab's publications page](research/publications/index.html). Information on funded research projects is available on [our Funding page](research/projects/index.html).
{% endimage_with_text %}
<br>

{% image_with_text "images/labs/reachandtouch/landingpage/Kinarm.jpg", "Image: Simon Haigermoser", "left", "Our Methods" %}
We approach our research questions with various methods – [find out more!](research/methods/index.html)
{% endimage_with_text %}
<br>

{% image_with_text "images/labs/reachandtouch/landingpage/publications.jpg", "Image: Simon Haigermoser", "left", "Our Publications" %}
Looking for our Output? Check out our most recent publications at the bottom of this page. Find all our publications on [our Lab's publications page](research/publications/index.html), or if you're specifically interested in one team member, find all their publications on [their personal Team pages](members/index.html).
{% endimage_with_text %}
<br>

{% image_with_text "images/labs/reachandtouch/landingpage/open_science.jpg", "Image: Simon Haigermoser", "left", "Our work ethic: Open Science" %}
We practice open science. This means we [publish our data and code](research/openscience/index.html), and we often preregister our studies before they run. [See all our repositories](research/openscience/index.html) and contact us if you do not find what you are looking for.
{% endimage_with_text %}
<br>

{% image_with_text "images/labs/reachandtouch/landingpage/student_supervisor_interaction.jpg", "Image: Simon Haigermoser", "left", "Students - join our lab!" %}
If you're thinking about joining us for your BSc- or MSc-thesis, check out our [detailed info on how things work in our group](students/index.html). Contact us by email or come by our offices if you're interested!
{% endimage_with_text %}

<br>

## What's new
{{ news.news(['reachandtouch'], include_images=True, max_articles=6) }}
