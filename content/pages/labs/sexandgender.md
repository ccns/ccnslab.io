---
Title: Sex Hormones and Gender Neuroscience
short_title: Sex & Gender Lab
Order: 5
---

{% import 'news.html' as news with context %}

## Research focus

### Why gender neuroscience?

Many findings about our brains come from studies on males. There are five times more neuroscientific studies with purely male samples than purely female samples (Mamlouk et al., 2017). However, it is becoming increasingly apparent that not all findings obtained in males can be applied one-to-one to females. 

On the one hand, this puts women[^1] at a disadvantage, because the less is knwon about their healthy brain, the more difficult it is to find adequate treatment methods in case of illness. There are numerous neurological and psychiatric diseases with different prevalence in males and females and where sex and gender play a significant role in diagnosis and treatment (gender medicine). These include, for example, autism, mood disorders or Alzheimer's disease. 

On the other hand, this situation is also dangerous for neuroscience itself, since hypotheses are generated for mixed-sex/mixed-gender samples that were obtained on the basis of purely male samples. Until it has been verified that the underlying assumptions hold equally for all sexes/genders, the results of mixed samples cannot be adequately interpreted. In many cases, this remains to be investigated. Thus, **gender** **neuroscience**, i.e. the study of sex and gender differences in neuroscientific findings, is a major focus of our research group. 

### Why sex hormones?

Historically, one reason for excluding females from (neuro-)scientific studies has been the assumption that, unlike males, they are subject to hormonal fluctuations (e.g. menstrual cycle) that can affect the brain, making it difficult to interpret results. However, it is fatal to medical care for all sexes/genders, if females are generally perceived as "unstable", while hormonal changes in men are completely ignored. 

On the one hand, research on how much the fluctuations of female hormones actually affect the brain (menstrual cycle research), is still in its infancy. On the other hand, men also experience significant hormonal flucutations, the effects of which on the brain and behaviour have hardly been studied at all and are neglected both in the interpretation of research results and in medicine. Therefore, we investigate in all sexes/genders, how the brain responds to periods of hormonal change and whether there are individuals whose brains are particularly sensitive to such changes.

### Links

- You want to learn more about our research? Check out our [ongoing research projects]({filename}sexandgender/research/projects.md)

- You want to keep up to date with our ongoing activities? Follow us on [Facebook](https://de-de.facebook.com/AGHormonGehirn/) and [Instagram](https://www.instagram.com/das.weibliche.gehirn/?hl=de)

### Are you interested in participating in one of our studies? Sign up [here](https://umfrage.sbg.ac.at/index.php/331818)

### News
{{ news.news(['sexandgender'], include_images=True) }}

[^1]: Since we are interested in both the biological and the psycho-social effects of sex/gender on the brain, the term "woman" here is to be understood as maximally inclusive and encompasses both individuals who were assigned female sex at birth and individuals who identify - at least partially - as women. There is need for research on all sexes/genders regardless of whether the concept is understood biologically, psychologically or socially and which classification is made in each case. Therefore we always survey sex/gender maximally differentiated including sex assigned at birth, gender identity, gender role and sexual orientation.
