---
Title: Ongoing Projects
Order: 1
---

{% message color="is-info", size="is-medium" %}
Got curious? Found a project you want to be a part of? [Sign up as a participant](https://umfrage.sbg.ac.at/index.php/331818)

Neugierig geworden? Wollen Sie an einer unserer Studien teilnehmen? [Melden Sie sich hier als Proband*in an](https://umfrage.sbg.ac.at/index.php/331818)
{% endmessage %}

{% from 'projects.html' import projects with context %}

{{ projects(tags=['sexandgender']) }}
