---
Title: Open Science
Order: 3
---

## Open hypotheses, data, and code
We support open science, preregistration & preprints. Here you can find published code and data. In more recent [publications](research/publications/index.html), the supplementary material is linked directly in the articles. Some older links here are currently broken but will be updated soon.     

### 2022

- **Noachtar, Harris, Hidalgo-Lopez & Pletzer, 2022, Communications Biology: "Sex and strategy effects on brain activation during a 3D-navigation task"**
    - [Article](https://www.nature.com/articles/s42003-022-03147-9)
    - [Data and Code](https://osf.io/t3v7z/)
- **Harris, Hagg & Pletzer, 2022, Frontiers in Neuroscience: "Eye-Movements During Navigation in a Virtual Environment: Sex Differences and Relationship to Sex Hormones"**
    - [Article](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9100804/)
    - [Data and Code](https://osf.io/pc94v/)
- **Menting-Henry, Hidalgo-Lopez, Aichhorn, Kronbichler, Kerschbaum & Pletzer, 2022, Frontiers in Behavioral Neuroscience: "Oral Contraceptives Modulate the Relationship Between Resting Brain Activity, Amygdala Connectivity and Emotion Recognition – A Resting State fMRI Study"**
    - [Article](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8967165/)
    - [Data and Code](http://webapps.ccns.sbg.ac.at/OpenData/)

### 2021

- **Pletzer, Poppelaars, Klackl & Jonas, 2021, Stress: "The gonadal response to social stress and its relationship to cortisol"**
    - [Article](https://www.tandfonline.com/doi/full/10.1080/10253890.2021.1891220)
    - [Data and Code](https://osf.io/jwpa4/)
- **Hidalgo-Lopez, Zeidman, Harris, Razi & Pletzer, 2021, Communications Biology: "Spectral dynamic causal modelling in healthy women reveals brain connectivity changes along the menstrual cycle"**
    - [Article](https://www.nature.com/articles/s42003-021-02447-w)
    - [Data and Code](https://osf.io/23d7x/)
- **Hausinger & Pletzer, 2021, Behavioral Brain Research: "Sex hormones modulate sex differences and relate to hemispheric asymmetries in a divided visual field Navon task"**
    - [Article](https://www.sciencedirect.com/science/article/pii/S0166432821001698)
    - [Data and Code](http://webapps.ccns.sbg.ac.at/OpenData/)
- **Hidalgo-Lopez & Pletzer, 2021, Psychoneuroendocrinology: "Fronto-striatal changes along the menstrual cycle during working memory: Effect of sex hormones on activation and connectivity patterns"**
    - [Article](https://www.sciencedirect.com/science/article/pii/S030645302030531X)
    - [Data and Code](http://webapps.ccns.sbg.ac.at/OpenData/)
- **Pletzer, Scheuringer, Harris & Scherndl, 2021, Journal of Experimental Psychology: General: "The missing link: Global-local processing relates to number-magnitude processing in women."**
    - [Article](https://www.researchgate.net/profile/Belinda-Pletzer/publication/345922212_The_missing_link_Global-local_processing_relates_to_number-magnitude_processing_in_women/links/616586551eb5da761e86ee63/The-missing-link-Global-local-processing-relates-to-number-magnitude-processing-in-women.pdf)
    - [Data and Code](http://webapps.ccns.sbg.ac.at/OpenData/)

### 2020

- **Schulte, Hawelka & Pletzer, 2020, Physiology & Behavior: "Eye-movements during number comparison: Associations to sex and sex hormones"**
    - [Article](https://www.sciencedirect.com/science/article/pii/S0031938420304753)
    - [Data and Code](http://webapps.ccns.sbg.ac.at/OpenData/)
- **Hidalgo-Lopez, Zimmermann & Pletzer, 2020, Scientific Reports: "Intra-subject consistency of spontaneous eye blink rate in young women across the menstrual cycle"**
    - [Article](https://www.nature.com/articles/s41598-020-72749-2)
    - [Data and Code](http://webapps.ccns.sbg.ac.at/OpenData/)
- **Scheuringer, Harris & Pletzer, 2020, Brain & Language: "Recruiting the right hemisphere: Sex differences in inter-hemispheric communication during semantic verbal fluency"**
    - [Article](https://www.sciencedirect.com/science/article/pii/S0093934X20300730)
    - [Data and Code](http://webapps.ccns.sbg.ac.at/OpenData/)
- **Hidalgo-Lopez, Mueller, Harris, Aichhorn, Sacher & Pletzer, 2020, Brain Structure & Function: "Human menstrual cycle variation in subcortical functional brain connectivity: a multimodal analysis approach"**
    - [Article](https://link.springer.com/article/10.1007/s00429-019-02019-z)
    - [Data and Code](http://webapps.ccns.sbg.ac.at/OpenData/)
- **Gruber, Distlberger, Scherndl, Ortner & Pletzer, 2020, European Journal of Psychological Assessment: "Psychometric Properties of the Multifaceted Gender-Related Attributes Survey (GERAS)"**
    - [Article](https://psycnet.apa.org/fulltext/2019-54260-001.html)
    - [Data and Code](https://osf.io/42jhr/)

### 2019

- **Harris, Scheuringer & Pletzer, 2019, Biology of Sex Differences: "Perspective and strategy interactively modulate sex differences in a 3D navigation task"**
    - [Article](https://bsd.biomedcentral.com/articles/10.1186/s13293-019-0232-z)
    - [Data and Code](http://webapps.ccns.sbg.ac.at/OpenData/)
- **Pletzer, Harris, Scheuringer & Hidalgo-Lopez, 2019, Neuropsychopharmacology: "The cycling brain: menstrual cycle related fluctuations in hippocampal and fronto-striatal activation and connectivity during cognitive tasks"**
    - [Article](https://www.nature.com/articles/s41386-019-0435-3)
    - [Data and Code](http://webapps.ccns.sbg.ac.at/OpenData/)
- **Pletzer, Jaeger & Hawelka, 2019, Neuropsychopharmacology: "Sex hormones and number processing. Progesterone and testosterone relate to hemispheric asymmetries during number comparison"**
    - [Article](https://www.sciencedirect.com/science/article/pii/S0018506X19300789)
    - [Data and Code](https://data.mendeley.com/datasets/h3zwdz4dx6/1)
- **Hidalgo-Lopez & Pletzer, 2019, Scientific Reports: "Individual differences in the effect of menstrual cycle on basal ganglia inhibitory control"**
    - [Article](https://www.nature.com/articles/s41598-019-47426-8)
    - [Data and Code](http://webapps.ccns.sbg.ac.at/OpenData/)
- **Pletzer, Harris & Hidalgo-Lopez, 2019, Scientific Reports: "Previous contraceptive treatment relates to grey matter volumes in the hippocampus and basal ganglia"**
    - [Article](https://www.nature.com/articles/s41598-019-47446-4)
    - [Data and Code](http://webapps.ccns.sbg.ac.at/OpenData/)

### 2018

- **Pletzer, Harris & Hidalgo-Lopez, 2018, Scientific Reports: "Subcortical structural changes along the menstrual cycle: beyond the hippocampus"**
    - [Article](https://www.nature.com/articles/s41598-018-34247-4)
    - [Data and Code](http://webapps.ccns.sbg.ac.at/OpenData/)

### 2017 

- **Pletzer, Scheuringer & Scherndl, 2017, Scientific Reports: "Global-local processing relates to spatial and verbal processing: implications for sex differences in cognition"**
    - [Article](https://www.nature.com/articles/s41598-017-11013-6)
    - [Data and Code](http://webapps.ccns.sbg.ac.at/OpenData/)

### 2016

- **Pletzer, 2016, Scientific Reports: "Sex differences in number processing: Differential systems for subtraction and multiplication were confirmed in men, but not in women"**
    - [Article](https://www.nature.com/articles/srep39064)
    - [Data and Code](http://webapps.ccns.sbg.ac.at/OpenData/)                
