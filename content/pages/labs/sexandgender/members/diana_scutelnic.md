---
Title: Diana Scutelnic
Date: 23.09.2022
Name: Diana
Surname: Scutelnic
Position: BTA
Tags: ccns, sexandgender
Email: diana.scutelnic@plus.ac.at
Phone: +4366280445171
Photo: diana_scutelnic.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---

## Background

I have studied Biochemistry and Biotechnology and continued my studies by acquiring a Master of Science in Molecular Biology and Genetics. My fields of expertise are molecular and cell biology as well as genetics, with focus on regulation of gene expression, protein biology and RNA biology. 

## Current Research Focus

Currently, my position as a biological technical assistant in Prof. Belinda Pletzer’s group, involves the genetic analysis of steroid receptors as well as the quantification of steroid and peptide hormones employing immunoassays and liquid-chromatography mass spectrometry.

An overwhelming percentage of women worldwide are currently using contraceptives. The [BECONTRA](https://www.europeandissemination.eu/becontra/11695) project is essential in illuminating the effects of contraceptives on cognition and I am happy to be assisting in such an important task, as the effects of oral contraceptives on the female brain and behavior are still not comprehensively known. 

## Publications

Maragozidis, P., Papanastasi, E., **Scutelnic, D.**, Totomi, A., Kokkori, I., Zarogiannis, S. G., ... & Balatsos, N. A. (2015). Poly (A)-specific ribonuclease and Nocturnin in squamous cell lung cancer: prognostic value and impact on gene expression. Molecular cancer, 14(1), 1-12.
