---
Title: Belinda Pletzer
Date: 20.06.2022
Name: Belinda
Surname: Pletzer
Position: Full Professor
Tags: ccns, sexandgender
Email: belinda.pletzer@plus.ac.at
Phone: ' +4366280445184'
Photo: belinda_pletzer.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Pure:
  id: 2592688
  types:
      article
      preprint
      shortreport
      review
      comment
  size: 100
---

## Background

Via my master studies at the Paris-Lodron-University of Salzburg, I obtained an inter-disciplinary background comprised of biology, psychology, philosophy and mathematics. I obtained two PhDs in Philosophy and Cognitive Neuroscience, respectively, and spent my first PostDoc year at the University of California, Irvine, researching the effects of sex and sex hormones on processing strategies during cognitive tasks. After returning to Austria, I followed up on this question including brain parameters within the framework of a single investigators project funded by the Austrian Science Fund. The results were summarized in my habilitation thesis in Psychology. 

## Current Research Focus

My research interests comprise all aspects of hormone-brain interactions, with a particular focus on sex hormones and gender-related aspects of neuroscience. I am currently the PI of the ERC Starting Grant [BECONTRA](https://www.europeandissemination.eu/becontra/11695), which investigates the effects of hormonal contraceptives on the female brain.