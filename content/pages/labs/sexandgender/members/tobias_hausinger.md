---
Title: Tobias Hausinger
Date: 23.09.2022
Name: Tobias
Surname: Hausinger
Position: PhD Student
Tags: ccns, sexandgender
Email: tobias.hausinger@plus.ac.at
Phone: +4366280445178
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---

## Background

I completed my master’s degree with a specialization in “Cognitive Neuroscience” at the University of Salzburg and started my PhD in 2021. 

## Current Research Focus

Despite wide-ranging interactions with the neural system, research on how the endocrine system influences cognitive processes is still in its infancy, making hormones one of the sleeping giants in neuroscience. My current project focuses on sex hormonal relations to hemispheric asymmetries during various domains of visuospatial processing.

## Publications

**Hausinger, T.**, & Pletzer, B. (2021). Sex hormones modulate sex differences and relate to hemispheric asymmetries in a divided visual field Navon task. Behavioural Brain Research, 408, 113281.
