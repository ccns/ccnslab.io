---
Title: Esmeralda Hidalgo-Lopez
Date: 20.06.2022
Name: Esmeralda
Surname: Hidalgo-Lopez
Position: PostDoc
Tags: ccns, sexandgender
Email: esmeralda.hidalgolopez@plus.ac.at
Phone: +4366280445171
Photo: esmeralda_hidalgo.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---

## Background 

I am a clinical psychologist with an MSc. in Neuroscience by the Universidad Autónoma de Madrid. I obtained my PhD at the University of Salzburg under the supervision of Prof. Belinda Pletzer. My doctoral thesis focused on the impact of ovarian hormones on human cognition and brain functioning. 

## Current Research Focus

As a PostDoc researcher I am currently involved in projects particularly related to women’s health, such as the impact of hormonal contraceptives on the female brain or premenstrual dysphoric disorder. Nowadays I focus on neuroimaging methods to seek mechanistic insights into the effects of ovarian and synthetic hormones on the brain and related mood effects and behaviour.

## [Publications](https://scholar.google.at/citations?hl=de&user=_j0RzSwAAAAJ)

**Hidalgo-Lopez, E.**, Engman, J., Poromaa, I. S., Gingnell, M., & Pletzer, B. (2022). Triple network model of brain connectivity changes related to adverse mood effects in an oral contraceptive placebo-controlled trial. medRxiv.

Noachtar, I., Harris, T. A., **Hidalgo-Lopez, E.**, & Pletzer, B. (2022). Sex and strategy effects on brain activation during a 3D-navigation task. Communications biology, 5(1), 1-14.

Menting-Henry, S., **Hidalgo-Lopez, E.**, Aichhorn, M., Kronbichler, M., Kerschbaum, H., & Pletzer, B. (2022). Oral contraceptives modulate the relationship between resting brain activity, amygdala connectivity and emotion recognition–a resting state fMRI study. Frontiers in Behavioral Neuroscience, 16.

**Hidalgo-Lopez, E.**, Zeidman, P., Harris, T., Razi, A., & Pletzer, B. (2021). Spectral dynamic causal modelling in healthy women reveals brain connectivity changes along the menstrual cycle. Communications biology, 4(1), 1-14.

**Hidalgo-Lopez, E.**, & Pletzer, B. (2021). Fronto-striatal changes along the menstrual cycle during working memory: effect of sex hormones on activation and connectivity patterns. Psychoneuroendocrinology, 125, 105108.

**Hidalgo-Lopez, E.**, Zimmermann, G., & Pletzer, B. (2020). Intra-subject consistency of spontaneous eye blink rate in young women across the menstrual cycle. Scientific reports, 10(1), 1-8.

**Hidalgo-Lopez, E.**, Mueller, K., Harris, T., Aichhorn, M., Sacher, J., & Pletzer, B. (2020). Human menstrual cycle variation in subcortical functional brain connectivity: a multimodal analysis approach. Brain Structure and Function, 225(2), 591-605.

**Hidalgo-Lopez, E.**, & Pletzer, B. (2019). Individual differences in the effect of menstrual cycle on basal ganglia inhibitory control. Scientific Reports, 9(1), 1-11.

Pletzer, B., Harris, T. A., Scheuringer, A., & **Hidalgo-Lopez, E.** (2019). The cycling brain: menstrual cycle related fluctuations in hippocampal and fronto-striatal activation and connectivity during cognitive tasks. Neuropsychopharmacology, 44(11), 1867-1875.

Pletzer, B., Harris, T., & **Hidalgo-Lopez, E.** (2019). Previous contraceptive treatment relates to grey matter volumes in the hippocampus and basal ganglia. Scientific Reports, 9(1), 1-8.

Pletzer, B., Harris, T., & **Hidalgo-Lopez, E.** (2018). Subcortical structural changes along the menstrual cycle: beyond the hippocampus. Scientific reports, 8(1), 1-6.

**Hidalgo-Lopez, E.**, & Pletzer, B. (2017). Interactive effects of dopamine baseline levels and cycle phase on executive functions: The role of progesterone. Frontiers in neuroscience, 11, 403.
