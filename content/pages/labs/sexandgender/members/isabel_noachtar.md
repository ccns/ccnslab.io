---
Title: Isabel Noachtar
Date: 23.09.2022
Name: Isabel
Surname: Noachtar
Position: PhD Student
Tags: ccns, sexandgender
Email: isabel.noachtar@plus.ac.at
Phone: +436648525499
Photo: isabel_noachtar.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---

## Background

I obtained my Master of Science in Psychology at the University of Salzburg in 2019 with a specialization in cognitive neuroscience. My fascination for scientific research grew during my work on a project about patients with traumatic brain injuries at a neuropsychological office in San Francisco. 

## Current Research Focus

In 2019, I joined Prof. Belinda Pletzer’s working group as well as the Doctoral College „Imaging the Mind“ and my work currently focuses on the effects of oral contraceptives on spatial cognition. During my PhD research, I strive to gain insight into the interactive effects of exogenous and endogenous sex hormones on cognitive processing. 

For me, the [BECONTRA](https://www.europeandissemination.eu/becontra/11695) project is a huge possibility to elucidate the effects of oral contraceptives on cognition, especially regarding spatial and verbal functioning and provide women with more comprehensive, evidence-based information for choosing contraceptives. As a psychologist I enjoy working in an interdisciplinary working group with the focus on biological psychology and it motivates me to expand my knowledge regarding research fields like steroid receptor analysis, endocrinology as well as neuroimaging.

## Publications

**Noachtar, I.**, Harris, TA., Hidalgo-Lopez, E., & Pletzer, B. (2022). Sex and strategy effects on brain activation during a 3D-navigation task. Communications biology, 5(1), 1-14.

**Noachtar, I.**, Hidalgo-Lopez, E., & Pletzer, B. (2022). Duration of oral contraceptive use relates to cognitive performance and brain activation in current and past users. Frontiers in Endocrinology, 13.
