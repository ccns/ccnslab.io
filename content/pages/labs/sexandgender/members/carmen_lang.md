---
Title: Carmen Lang
Date: 23.09.2022
Name: Carmen
Surname: Lang
Position: PhD Student
Tags: ccns, sexandgender
Email: carmen.lang@plus.ac.at
Phone: +436648525499
Photo: carmen_lang.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---

## Background

I completed my Bachelor´s Degree in Psychology at the Friedrich-Alexander-Universität in Erlangen-Nürnberg and am currently completing my Master´s Degree in Psychology with a specialisation in Cognitive Neuroscience at the Paris-Lodron-University in Salzburg. During my master studies at the university of Salzburg I worked as a student assistant in Prof. Belinda Pletzer's working group. 

## Current Research Focus

My current project focuses on the effect of oral contraception on functional connectivity at rest.

The influences of hormonal contraception on our brain is a field of research that is largely overlooked. I particularily enjoy being involved in every part of the research process and our team´s steps to close this gap, while continually learning about the brain in its endocrine context.

## Publications

Pletzer, B., **Lang, C.**, Derntl, B., & Griksiene, R. (2022). Weak associations between personality and contraceptive choice. Frontiers in Neuroscience.
