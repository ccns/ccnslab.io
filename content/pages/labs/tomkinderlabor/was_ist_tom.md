---
Title: Was ist "Theory of Mind"?
Order: 1
---
## Was ist "Theory of Mind"? 
Theory of Mind (ToM) ist eine Bezeichnung für die menschliche Fähigkeit über die Bewusstseinsvorgänge anderer Personen, z.B. deren Gefühle, Bedürfnisse, Ideen, Absichten, Erwartungen und Meinungen, nachzudenken und daraus ihre Handlungsgründe zu erschließen.

Erst wenn wir die Wünsche, Ziele und Überzeugungen anderer Menschen berücksichtigen, wird deren Verhalten für uns versteh- und vorhersagbar. Prof. Dr. Josef Perner ist ein renommierter Entwicklungs- und Kognitionspsychologe und hat dieses Forschungsgebiet maßgeblich etabliert.

Wissenschaftliche Erkenntnisse über den kindlichen Entwicklungsverlauf sind immer dann von großer Bedeutung, wenn die Entwicklung vom erwarteten Verlauf stark abweicht. Diagnose- und Therapiemethoden können auf Basis von Forschungsergebnissen gezielt entwickelt werden.

Wir liefern mit unserer Forschung die Grundlage dafür und sind dabei auf die Teilnahme von Kindern an unseren Studien angewiesen. Unzählige Eltern und Kindereinrichtungen haben uns dankenswerterweise in den letzten Jahren bereits dabei unterstützt.