---
Title: Aktuelle Projekte
Subtitle: 
Order: 3
---


## Aktuelle Projekte 

**[Zu dir hüpf ich (nicht)]({filename}aktuelle_projekte/zu_dir_huepf_ich_(nicht).md)**

- Machen einjährige Kinder Kosten-Nutzen-Rechnungen?
- Altersbereich: 12 – 14 Monate
- Dauer des Besuchs: ca. 30 Minuten

**[Es rappelt in der Kiste]({filename}aktuelle_projekte/es_rappelt_in_der_kiste.md)**

- Warum beginnen Kinder mit 1,5 Jahren, spontan anderen zu helfen?
- Altersbereich: 15 – 30 Monate
- Dauer des Besuchs: ca. 45 Minuten


## Abgeschlossene Projekte

**[Ich sehe was, was du nicht siehst…]({filename}aktuelle_projekte/ich_sehe_was_was_du_nicht_siehst.md)**

- Wie lernen Kinder, sich in andere Personen hinein zu versetzen?
- Altersbereich: 18 – 27 Monate
- Dauer des Besuchs: ca. 30 Minuten