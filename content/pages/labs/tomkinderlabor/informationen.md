---
Title: Informationen
Subtitle: 
Order: 4
---


## Wegbeschreibung
Unsere Forschungsräume befinden sich an der Paris-Lodron-Universität Salzburg, im Fachbereich der Psychologie, an der Naturwissenschaftlichen Fakultät in der Hellbrunnerstraße 34.

Sie können uns sowohl mit öffentlichen Verkehrsmitteln als auch mit dem Auto problemlos erreichen.

Parkplätze sind auf der Michael-Pacher-Straße (direkt vor der Fakultät) für 3 Stunden kostenlos belegbar. Falls Sie Probleme haben, uns oder einen Parkplatz zu finden, rufen Sie uns an. Wir helfen Ihnen gerne weiter.

An der Naturwissenschaftlichen Fakultät gehen Sie durch das Tor über den Vorplatz zum Springbrunnen. Hier werden Sie herzlich von einem Mitarbeiter des Kinderlabor-Teams in Empfang genommen!

### Anreise mit dem Auto/ zu Fuß

Sie erreichen die Hellbrunnerstraße 34 über die Alpenstraße → Michael-Pacher-Straße oder über die Hofhaymer Allee.

Zu Fuß kommen Sie auch über den schönen Freisaalweg zu uns.

### Anreise mit dem Bus

Mit der  Obus-Linie 3 oder 8 zur Haltestelle Faistauergasse und danach zu Fuß nach ca. 200 Metern rechts in die Michael-Pacher-Straße → 300 Meter geradeaus.

Oder mit der Bus-Linie 22 zur Haltestelle Michael-Pacher-Straße → hält direkt vor dem Gebäude.



## Fragen zum Datenschutz

### Welche Daten benötigen wir von den einzelnen Kindern?

Wir benötigen lediglich den Vornamen und das Geburtsdatum der Kinder, die an den Studien teilnehmen. Manchmal bitten wir auch um weitere Informationen, wenn diese für die Fragestellung von Interesse sind, z.B. Anzahl und Alter von Geschwisterkindern.
Warum werden diese personenbezogenen Daten erhoben?

Der Vorname des Kindes ist wichtig, um bei der Durchführung des Experiments eine möglichst natürliche Atmosphäre für das Kind zu schaffen. Um Aussagen über alterstypische Verläufe der kindlichen Entwicklung von Theory of Mind Fähigkeiten machen zu können, brauchen wir für unsere Studien das genaue Alter der Kinder.

### Wer hat Zugang zu  den personenbezogene Daten und wie lange werden diese gespeichert?

Alle Daten, die wir von Ihrem Kind aufzeichnen, werden mit einem Code versehen und so in anonymisierter Form weiterverwendet. Ihrem Kind wird dabei eine Versuchspersonennummer zugewiesen, die rein zufällig zustande kommt. Daraus ist nicht nachvollziehbar, um welches Kind es sich handelt. Formulare mit dem Namen Ihres Kindes und eine Tabelle, die den Namen mit dem Code verbindet, werden von der Untersuchungsleiterin oder dem Untersuchungsleiter in einem abgeschlossenen Schrank bzw. auf einem passwortgeschützten Computer der Universität Salzburg verwahrt und werden strikt vertraulich behandelt. Der Name Ihres Kindes oder Informationen, die Dritten erlauben könnten, die Identität Ihres Kindes zu ermitteln, treten an keiner anderen Stelle mehr auf. Die Resultate der Studie werden anonymisiert ausgewertet und anonymisiert an andere Wissenschaftler oder die Öffentlichkeit kommuniziert.

Die individuelle Versuchspersonennummer Ihres Kindes finden Sie auf der Einverständniserklärung. Diese erlaubt es Ihnen auch nach Abschluss der Studie Einsicht in die Daten ihre Kindes zu nehmen und/oder deren Löschung zu verlangen.

### Warum sind Videoaufzeichnungen nötig?

Zur genauen Auswertung und um die Nachvollziehbarkeit der Ergebnisse zu gewährleisten, ist es notwendig Videoaufnahmen von der Untersuchungssituation zu machen. Eine Kopie dieser Aufnahmen stellen wir Ihnen auf Wunsch gerne zur Verfügung (DVD oder zeitlich beschränkter und Passwort-geschützter Download-Link).

Videoaufzeichnungen werden ausschließlich mit der Versuchspersonennummer benannt und zunächst kurzfristig auf der SD-Karte der Kamera gespeichert und später auf einem abgesicherten Server der Universität Salzburg archiviert. Die Aufnahmen werden ausschließlich von den VersuchsleiterInnen und MitarbeiterInnen des Kinderlabors zur projektbezogenen wissenschaftlichen Auswertung angesehen und sind Dritten keinesfalls zugänglich. Nur wenn Sie nach der Untersuchung Ihre ausdrückliche Zustimmung geben, wird die Aufnahme eventuell auch zu Demonstrationszwecken in Lehrveranstaltungen der Universität Salzburg bzw. bei wissenschaftlichen Fachkongressen verwendet.

### Was passiert weiter mit den Untersuchungsergebnissen?

Auf Basis der erhobenen Daten verfassen wir wissenschaftliche Arbeiten. Datenfile, Protokollbögen und ggf. Videoaufzeichnungen werden von der Untersuchungsleiterin aufbewahrt. Nach spätestens 10 Jahren werden die gesamten Daten gelöscht/vernichtet.

### Werden die  Eltern die allgemeinen Ergebnisse der Studie erfahren?

Selbstverständlich bekommen die Eltern, die Interesse bekundet haben, ein Rückmeldemail in dem Ziele und Ergebnisse der Studie noch einmal zusammengefasst dargestellt sind. (Die Ergebnisse liegen allerdings erst nach Abschluss der Datenerhebung und Auswertung vor, dies kann mehrere Monate dauern.)

### Besprechen wir die individuellen Ergebnisse eines Kindes mit den Eltern?

Wir untersuchen allgemeine Entwicklungszusammenhänge und die Daten der einzelnen Kinder sind daher nur im Zusammenhang einer großen Stichprobe aussagekräftig. Die von uns verwendeten Methoden sind nicht geeignet, um Aussagen über einzelne Kinder zu treffen. Auf Wunsch können wir jedoch die Eindrücke der Testsituation mit Ihnen besprechen. Falls wir bei Ihrem Kind stark abweichende Ergebnisse bemerken, werden wir Ihnen dies auf Wunsch mitteilen.


## Häufig gestellte Fragen

### Was wird untersucht und worum geht es?

Wir stellen uns die Frage „Wie verstehen kleine Kinder, was andere wissen und wollen?“. Erst wenn wir die Wünsche, Ziele und Überzeugungen anderer Menschen berücksichtigen, wird deren Verhalten für uns versteh- und vorhersagbar. Wie und ab wann können Kinder die Handlungen anderer Menschen aus deren Perspektive nachvollziehen? Details dazu unter Was ist „Theory of Mind“?.

### Was ist eine Blickbewegungsstudie?

Dabei zeichnet eine Kamera die Augenbewegungen des Kindes auf, während es kurze Videosequenzen sieht. Die Videos sind meist von uns selbst gedreht oder animiert, beinhalten soziale Interaktionen und dauern nicht länger als 2-3 Minuten. Die dargebotenen Szenen können ganz entspannt, wie beim Fernsehen, angeschaut werden. Weder eine Kopffixierung noch zusätzliche Apparaturen am Kopf werden benötigt.

Die aufgezeichneten Blickbewegungen werden später statistisch ausgewertet. So kann man ganz genau erkennen, was sich das Kind angeschaut hat. Hat es auf die Gesichter geachtet? Auf die Hände? Auf sich bewegende Dinge? Welche Aspekte der dargestellten Szene werden wann, wie lange und in welcher Reihenfolge mit den Augen verfolgt?

Die Blickmuster liefern wichtige Anhaltspunkte und interessante Einblicke über das kindliche Verständnis von sozialen Situationen.

### Wer kann teilnehmen?

Teilnehmen können Kinder zwischen 9 Monaten und 6 Jahren.

Falls Sie aus irgendeinem Grund unsicher sind, ob die Teilnahme für Ihr Kind gut ist bzw. ob Ihr Kind für die Studie geeignet ist, besprechen Sie das am besten mit uns persönlich.

### Wo findet das statt?

Das Kinderlabor ist ein speziell für Kinder in diesem Alter einladend gestalteter Raum an der Naturwissenschaftlichen Universität Salzburg am Fachbereich Psychologie in der Hellbrunnerstrasse 34. Wir holen Sie am Eingang ab und begleiten Sie zum Laborraum. Details zur Anfahrt finden sie unter Wegbeschreibung.

### Wie kann ich uns anmelden?

Bei Interesse, an einer unserer Studien teilzunehmen oder falls Sie weitere Fragen haben, melden Sie sich einfach telefonisch (Telefonnummer: 0681 813 090 29) oder per E-Mail ( kinderlabor@plus.ac.at ) und fragen Sie unverbindlich nach, ob gerade ein Projekt läuft, das für die Altersgruppe Ihres Kindes passt. Sollten Sie einmal verhindert sein, besteht jederzeit die Möglichkeit, die Anmeldung telefonisch oder schriftlich wieder zurück zu nehmen.

### Wie lange dauert ein Besuch im Kinderlabor?

Das hängt von der jeweiligen Studie und dem jeweiligen Kind ab. In der Regel dauert ein Besuch bei uns insgesamt nicht länger als 30 Minuten, wobei die Versuche an sich viel kürzer sind und je nach Alter der Kinder zwischen 5 und 20 Minuten dauern.

### Was ist, wenn mein Kind weint oder sich nicht wohl fühlt?

Es kommt vor, dass ein Kind gerade gar keine Lust hat, mitzumachen oder dass es aus anderen Gründen anfängt zu quengeln oder zu weinen. Dafür haben wir volles Verständnis. Wer mit kleinen Kindern arbeitet, ist auf solche Situationen vorbereitet. Außerdem kann die Studie auch jederzeit unterbrochen werden.

### Gibt es die Möglichkeit, mein Kind während des Besuchs im Kinderlabor zu wickeln oder zu füttern?

Wenn Sie merken, dass Ihr Kind Hunger hat oder gewickelt werden muss, kann der Ablauf jederzeit unterbrochen werden (Wickelgelegenheit ist vorhanden).

### Dürfen Geschwisterkinder mitkommen?

Damit ein Kind auch zeigen kann was es schon kann, ist es wichtig, dass es nicht abgelenkt wird. Aus diesem Grund ist es ungünstig, wenn andere Kinder im Raum sind.

Für Geschwisterkinder die alt genug sind können wir aber bei Bedarf eine extra Betreuung organisieren (bitte vorher mit uns vereinbaren).

### Was bekomme ich für eine Teilnahme?

Mit Ihrer Teilnahme unterstützen Sie die wissenschaftliche Forschung an der Universität Salzburg und leisten einen wichtigen Beitrag zum Verständnis der Entwicklung von Kindern. Sie können Ihr Kind in einer interessanten Situation beobachten und psychologische Forschung hautnah miterleben.

Damit Sie und Ihr Kind eine bleibende Erinnerung an Ihren Besuch bei uns haben, gibt es zum Abschluss als Dankeschön ein kleines Geschenk und eine Urkunde. Außerdem wollen wir Sie für Ihren Aufwand entschädigen, z.B. in Form eines Eintrittsgutscheines für das Salzburger Spielzeug Museum, das hoppolino oder den Zoo Salzburg.

### Was passiert mit meinen persönlichen Angaben, die ich bei der Anmeldung zu einer Studie gemacht habe? Welche Daten werden erhoben?

Ihre Adressen und Telefonnummern werden bei uns unter Verschluss gehalten, nicht weitergeben und auf Wunsch jederzeit gelöscht. Die Daten zu Ihnen oder zu Ihrem Kind werden nur in anonymisierter Form gespeichert und ausgewertet. Außer unseren Mitarbeitern hat niemand Zugang zu diesen Daten.

Genauere Informationen dazu finden Sie unter Fragen zum Datenschutz.

### Werde ich von den Ergebnissen der Studie erfahren?

Selbstverständlich bekommen die Eltern, die mit ihrem Kind an einer unserer Studien teilgenommen haben und Interesse bekundet haben, ein Rückmeldemail, in dem Ziele und Ergebnisse der Studie noch einmal zusammengefasst dargestellt sind. Die Ergebnisse liegen allerdings erst nach Abschluss der Datenerhebung und Auswertung vor, dies kann mehrere Monate dauern, weshalb wir Sie um etwas Geduld bitten müssen.

### Ich habe weitere Fragen – an wen kann ich mich wenden?

Gerne beantworten wir Ihre weiteren Fragen telefonisch (Telefonnummer: 0681 813 090 29) oder per E-Mail (kinderlabor@plus.ac.at).
