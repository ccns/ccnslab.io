---
Title: Anmeldung/Kontakt
subtitle:
summary:
order: 5
---
E-Mail: kinderlabor@plus.ac.at

**[Direktanmeldung ](mailto:kinderlabor@plus.ac.at?subject=Studienteilnahme %22Ich sehe was%2C was du nicht siehst...%22&body=Liebes Kinderlabor-Team%2C%0D%0A%0D%0Amein Sohn%2Fmeine Tochter und ich w%C3%BCrden gerne an der Studie %22Ich sehe was%2C was du nicht siehst...%22 teilnehmen.%0D%0AMein Kind hei%C3%9Ft %5BNAME DES KINDES%5D und ist %5BALTER DES KINDES%5D Monate alt.%0D%0A%0D%0ALiebe Gr%C3%BC%C3%9Fe%0D%0A%5BIHR NAME%5D
mailto:kinderlabor@plus.ac.at?subject=Studienteilnahme&body=Liebes Kinderlabor-Team%2C%0D%0A%0D%0Amein Sohn%2Fmeine Tochter und ich w%C3%BCrden gerne an der Studie %22Ich sehe was%2C was du nicht siehst...%22 %2F %22Ab wann verstehen Kinder%2C dass ein Ziel erstrebenswert ist%3F%22 teilnehmen.%0D%0AMein Kind hei%C3%9Ft %5BNAME DES KINDES%5D und ist %5BALTER DES KINDES%5D Monate alt.%0D%0A%0D%0ALiebe Gr%C3%BC%C3%9Fe%0D%0A%5BIHR NAME%5D)**<br> 

Telefon: 0681 813 090 29

**[facebook](https://www.facebook.com/ToMKinderlabor/)**<br>