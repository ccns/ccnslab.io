---
Title: Zu dir hüpf ich (nicht)
Order: 1
---

## Machen einjährige Kinder Kosten-Nutzen-Rechnungen?  

<div class="card">
    <div class="card-content">
        <i>Studienleitung:</i> Anna Krämer<br>
        <i>Studiendurchführung:</i> Anna Krämer, Fiona Walter, Laura Lauber, Amelie Peters<br>
        <i>Dauer des Besuchs:</i> ca. 30 Minuten<br>
        <i>Altersbereich:</i> 12 – 14 Monate<br>
    </div>
</div>



**[Hier können Sie Ihr Kind zur Studie „Zu dir hüpf ich (nicht)“ anmelden](mailto:kinderlabor@plus.ac.at?subject=Studienteilnahme %22Zu dir h%C3%BCpf ich (nicht)%22&body=Liebes Kinderlabor-Team%2C%0D%0A%0D%0Amein Sohn%2Fmeine Tochter und ich w%C3%BCrden gerne an der Studie %22Zu dir h%C3%BCpf ich (nicht)%22 teilnehmen.%0D%0AMein Kind hei%C3%9Ft %5BNAME DES KINDES%5D und ist %5BALTER DES KINDES%5D Monate alt.%0D%0A%0D%0ALiebe Gr%C3%BC%C3%9Fe%0D%0A%5BIHR NAME%5D)** 

 

## Worum geht's?
Wenn wir Handlungen anderer Menschen beobachten und versuchen diese zu verstehen oder vorherzusagen, machen wir manchmal sogenannte „Kosten-Nutzen-Rechnungen“. Ein Beispiel: Max nimmt viel Aufwand und Mühen auf sich, um seinen Freund Moritz zu besuchen. Daraus können wir schließen, dass ihm Moritz sehr wichtig ist. Wenn Max nun aber nicht den gleichen Aufwand auf sich nimmt, um bei Lehrer Lämpel vorbei zu schauen, ist uns klar, wen von beiden er lieber hat. Solche Überlegungen machen Erwachsene alltäglich und ganz intuitiv. Wir wollen jetzt herausfinden, ob auch einjährige Kinder bereits diese „Kosten-Nutzen-Rechnungen“ machen.  


## Wie läuft die Studie ab? 
In unserem „kleinen Kino“ zeigen wir Ihrem Kind einen kurzen, kinderfreundlichen Animationsfilm von einem lustigen roten Ball. Dieser Ball hüpft über unterschiedlich tiefe Gräben zu seinen Freunden—dem gelben Dreieck und dem blauen Viereck. Zu einem von den beiden hüpft der Rote jedoch nicht mehr, wenn der Graben zu tief wird. Anschließend hat der rote Ball die Möglichkeit, ohne einen Graben zum gelben Dreieck oder zum blauen Viereck zu gehen. Nun ist es für uns spannend, ob Kinder bereits erwarten, dass der Ball zu der Figur geht, für die er über den tieferen Graben gesprungen ist? 

Hierfür achten wir auf das Blickmuster des Kindes beim Betrachten des Videos. Daraus leiten wir ab, worauf sie ihre Aufmerksamkeit richten und wie sie erwarten, dass die Situation weitergehen wird. Die Augenbewegungen werden mit Hilfe einer speziellen Kamera (Tobii Eye-Tracker X3-120) aufgezeichnet. Sie verfolgt die Pupillen automatisch, sodass keinerlei zusätzliche Apparaturen notwendig sind. Die Videos werden wie beim Fernsehen am Bildschirm dargeboten und ganz entspannt angesehen, das Kind bemerkt die Aufnahme seiner Blickbewegungen nicht.

Sie als Eltern sind immer dabei, und können die Reaktionen Ihres Kindes direkt beobachten. Inklusive Vor- und Nachbesprechung sollten Sie für den Termin etwa 30 Minuten einplanen.


## Was habe ich davon?
Dank Ihrer Teilnahme an dieser Studie können wir etwas über die Entwicklung des Verständnisses für zielgerichtete Handlungen, insbesondere Kosten-Nutzen-Überlegungen, in der frühen Kindheit erfahren. 

Am Ende des Besuchs erhält jedes Kind ein kleines Geschenk und eine Urkunde als Dankeschön für die tolle Mitarbeit. Sie können sich außerdem einen Gutschein für eine Salzburger Kinder-Freizeiteinrichtung (z.B. Zoo, Spielzeugmuseum, Hoppolino) aussuchen. Bei Interesse können wir nach Abschluss der Studie Ihre individuellen Ergebnisse mit Ihnen persönlich besprechen. Selbstverständlich erfahren Sie auch die allgemeinen Ergebnisse der Studie. 


## Wie kann ich mitmachen?
Für die Teilnahme mit Ihrem Kind schreiben Sie einfach eine E-Mail an [kinderlabor@plus.ac.at](mailto:kinderlabor@plus.ac.at?subject=Studienteilnahme %22Zu dir h%C3%BCpf ich (nicht)%22&body=Liebes Kinderlabor-Team%2C%0D%0A%0D%0Amein Sohn%2Fmeine Tochter und ich w%C3%BCrden gerne an der Studie %22Zu dir h%C3%BCpf ich (nicht)%22 teilnehmen.%0D%0AMein Kind hei%C3%9Ft %5BNAME DES KINDES%5D und ist %5BALTER DES KINDES%5D Monate alt.%0D%0A%0D%0ALiebe Gr%C3%BC%C3%9Fe%0D%0A%5BIHR NAME%5D) oder kontaktieren Sie uns telefonisch bzw. per WhatsApp ([0681 813 090 29](tel:+4368181309029)). Bei der Terminvereinbarung sind wir flexibel und passen uns Ihrem individuellen Tagesablauf an.
Haben Sie noch offene Fragen? Dann kontaktieren Sie uns ebenfalls, wir beantworten diese gerne. <br>

<br>

Wir freuen uns auf Ihren Besuch im Theory of Mind-Kinderlabor! 

Anna, Fiona, Laura und Amelie