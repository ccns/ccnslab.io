---
Title: Ich sehe was, was du nicht siehst…
short_title: (Abgeschlossen) Ich sehe was, was du nicht siehst…
Order: 3
---

## Wie lernen Kinder, sich in andere Personen hinein zu versetzen?
<div class="card">
    <div class="card-content">
        <i>Studienleitung:</i> Josef Perner, Beate Priewasser, Anna Krämer<br>
        <i>Studiendurchführung:</i> Anna Krämer, Jessica Schiller, Lara Schulz, Amelie Peters<br>
        <i>Dauer des Besuchs:</i> ca. 30 Minuten<br>
        <i>Altersbereich:</i> 18 – 27 Monate
    </div>
</div>

<br>

<i>Liebe Eltern, 

Dank Ihrer Unterstützung konnten wir die Erhebungsphase für diese Studie erfolgreich abschließen! 
Wir sind gerade dabei, die Daten auszuwerten, und freuen uns, Ihnen bald die Ergebnisse dieser Studie zukommen lassen zu können.</i>

## Worum geht's?
Um andere Menschen besser verstehen zu können, versetzen wir uns in sie hinein. Wir versuchen herauszufinden, was eine Person denkt oder fühlt. So können wir ihr Verhalten erklären und vorhersagen. Diese wichtige Fähigkeit bezeichnet man als „Theory of Mind“. Sie entwickelt sich bei Kindern in den ersten Lebensjahren, doch leider wissen wir noch wenig darüber, wie Kinder sie erlernen.

Das wollen wir ändern! Deshalb führen wir im Theory of Mind-Kinderlabor der Universität Salzburg die spannende Laborstudie „Ich sehe was, was du nicht siehst…“ durch. Die Studie ist Teil des internationalen Forschungsprojekts “ManyBabies 2“, welches die Entwicklung der Fähigkeit, sich in andere Menschen hineinversetzen zu können, untersucht. Weitere Informationen dazu finden Sie hier:  **[ManyBabies](https://manybabies.github.io/MB2/.)**<br>

## Wie läuft die Studie ab?
Wir zeigen Ihrem Kind einen kurzen, kinderfreundlichen Animationsfilm von einer Maus und einem Bären, die fangen spielen. Die Maus krabbelt durch einen Tunnel mit zwei Ausgängen und versteckt sich dort in einer von zwei Boxen. Anschließend möchte der Bär die Maus finden und krabbelt hinterher. In manchen Videos hat der Bär gesehen, in welcher Box sich die Maus versteckt – in manchen allerdings nicht. Berücksichtigen die Kinder schon, ob der Bär weiß, wo sich die Maus versteckt?

Mit Hilfe einer Blickbewegungskamera erfassen wir, auf welchen Tunnel-Ausgang die Kinder schauen, noch bevor der Bär dort auftaucht. Das zeigt uns, wo die Kinder erwarten, dass der Bär nach der Maus suchen wird. Uns interessiert, ob sie unterschiedliche Erwartungen haben, je nachdem, was der Bär gesehen hat. Die Teilnahme an der Blickbewegungsaufnahme dauert für Ihr Kind ca. 10 Minuten. Die Augenbewegungen werden mit Hilfe einer speziellen Kamera (Tobii Eye-Tracker X3-120) aufgezeichnet. Sie verfolgt die Pupillen automatisch, sodass keinerlei zusätzliche Apparaturen notwendig sind. Der Film wird wie beim Fernsehen am Bildschirm dargeboten und Ihr Kind sitzt beim Zusehen auf Ihrem Schoß. Sie als Eltern sind also immer mit dabei!

Alle unsere Aufgaben haben spielerischen Charakter und sind liebevoll und kindgerecht gestaltet. Im Anschluss an den Film bitten wir Sie, einen kurzen, demographischen Fragebogen auszufüllen. Inklusive Vor- und Nachbesprechung sollten Sie für den Termin etwa 45 Minuten einplanen.

## Was habe ich davon?
Dank Ihrer Teilnahme an dieser Studie können wir etwas über die Entwicklung der wichtigen Fähigkeit zur „Theory of Mind“ in der frühen Kindheit erfahren. So helfen Sie uns besser zu verstehen, wie Kinder lernen, sich in andere Personen hineinzuversetzen.

Am Ende des Besuchs erhält jedes Kind eine Urkunde und ein kleines Geschenk als Dankeschön für die tolle Mitarbeit! Sie können sich außerdem einen Gutschein für eine Salzburger Kinder-Freizeiteinrichtung (z.B. Zoo, Spielzeugmuseum, Hoppolino) aussuchen. Bei Interesse können wir nach Abschluss der Studie Ihre individuellen Ergebnisse mit Ihnen persönlich besprechen.

## Wie kann ich mitmachen?
Für die Teilnahme mit Ihrem Kind schreiben Sie einfach eine E-Mail an kinderlabor@plus.ac.at oder kontaktieren Sie uns telefonisch (0681 813 090 29). Wir vereinbaren dann einen Termin, der in Ihren individuellen Tagesablauf passt.

Wir freuen uns auf Ihren Besuch im Theory of Mind-Kinderlabor!

Anna, Jessica, Lara, Amelie