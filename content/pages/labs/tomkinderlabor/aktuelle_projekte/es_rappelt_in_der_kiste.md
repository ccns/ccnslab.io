---
Title: Es rappelt in der Kiste
Order: 2
---

## Warum beginnen Kinder mit 1,5 Jahren, spontan anderen zu helfen? 

<div class="card">
    <div class="card-content">
        <i>Studienleitung:</i> Anna Krämer<br>
        <i>Studiendurchführung:</i> Anna Krämer, Fiona Walter<br>
        <i>Dauer des Besuchs:</i> ca. 45 Minuten<br>
        <i>Altersbereich:</i> 15 – 30 Monate<br>
    </div>
</div>



**[Hier können Sie Ihr Kind zur Studie „Es rappelt in der Kiste“ anmelden](mailto:kinderlabor@plus.ac.at?subject=Studienteilnahme %22Es rappelt in der Kiste%22&body=Liebes Kinderlabor-Team%2C%0D%0A%0D%0Amein Sohn%2Fmeine Tochter und ich w%C3%BCrden gerne an der Studie %22Es rappelt in der Kiste%22 teilnehmen.%0D%0AMein Kind hei%C3%9Ft %5BNAME DES KINDES%5D und ist %5BALTER DES KINDES%5D Monate alt.%0D%0A%0D%0ALiebe Gr%C3%BC%C3%9Fe%0D%0A%5BIHR NAME%5D)** 

 

## Worum geht's?
Kurz bevor Kinder 2 Jahre alt werden, passiert etwas Bemerkenswertes: Sie beginnen ganz spontan (und meist mit viel Freude!) anderen zu helfen. Warum sie das tun, ist aber noch unklar. In unserer Studie wollen wir herausfinden, was diesem Hilfeverhalten zugrunde liegt.

Wir vermuten, dass Kinder zwischen anderthalb und zwei Jahren anfangen zu helfen, weil sie Handlungen allgemein neu verstehen und interpretieren lernen: Und zwar so, dass sie das Ziel einer Handlung erstmals als etwas Gutes, Erstrebenswertes sehen. Denn wenn sie verstehen, dass ein Ziel erstrebenswert ist, habe sie selbst einen guten Grund dabei zu helfen, dass dieses Ziel erreicht wird. 

Ein Beispiel: Felix trägt einen Stapel Bücher zu einem Kasten, stößt gegen die Tür, kann diese aber – weil er die Hände voll hat – nicht öffnen. Die 20-monatige Verena beobachtet das eine Zeit, geht dann zum Kasten und öffnet die Türe. Felix kann seine Bücher nun wegräumen. 

Wir glauben, dass Verena nicht nur verstanden hat, dass Felix die Bücher in den Kasten räumen will. Wir glauben, dass sie auch versteht, dass es gut ist, wenn die Bücher im Kasten verräumt sind – oder zumindest besser, als wenn sie bei Felix auf dem Arm bleiben. Deswegen hilft sie dabei, dass die Bücher im Kasten landen. 

Mit Ihrer Hilfe wollen wir herausfinden, ob Helfen mit diesem „Wert-Verstehen“ von Handlungszielen zusammenhängt. 


## Wie läuft die Studie ab? 
Alle unsere Aufgaben haben spielerischen Charakter und sind liebevoll und kindgerecht gestaltet. 

Zuerst zeigen wir den Kindern zwei Kisten: in einer befinden sich Kekse (Mini-Leibniz Kekse oder Pufuleti), in der anderen nur Papierschnipsel. Anschließend sehen die Kinder einen kurzen, animierten Film (ca. 1 Minute), in dem ein Smiley-Ball zu diesen beiden Kisten rollt, sich für eine entscheidet und darin verschwindet. In einer anderen Aufgabe (ohne Video) schaut die Versuchsleiterin in zwei Kisten und nimmt sich aus einer der beiden etwas heraus. Beide male fragen wir die Kinder, wo wohl die Kekse sind? Spannend für uns ist, wie sie sich entscheiden! Wenn sie schon erkennen, dass das Ziel einer absichtlichen Handlung erstrebenswert ist, sollten sie eher die Kiste wählen, die auch der Smiley-Ball bzw. die Versuchsleiterin gewählt hat. 

Um den Zusammenhang mit dem Hilfeverhalten zu betrachten, zeigen wir den Kindern, wie eine etwas tollpatschige Versuchsleiterin an verschiedenen Vorhaben scheitert (z. B. etwas zeichnen, aber der Stift fällt runter; Spielzeuge einwickeln, aber das Papier geht aus). Die Kinder können ihr helfen, indem sie Gegenstände reichen, ihr etwas zeigen oder einen Teil der Aufgabe für sie lösen. 

Sie als Eltern sind immer dabei, und können die Reaktionen Ihres Kindes direkt beobachten. Inklusive Vor- und Nachbesprechung sollten Sie für den Termin etwa 45 Minuten einplanen.


## Was habe ich davon?
Dank Ihrer Teilnahme an dieser Studie können wir etwas über die Entwicklung und die kognitiven Grundlagen von Hilfeverhalten in der frühen Kindheit erfahren. 

Am Ende des Besuchs erhält jedes Kind ein kleines Geschenk und eine Urkunde als Dankeschön für die tolle Mitarbeit. Sie können sich außerdem einen Gutschein für eine Salzburger Kinder-Freizeiteinrichtung (z.B. Zoo, Spielzeugmuseum, Hoppolino) aussuchen. Bei Interesse können wir nach Abschluss der Studie Ihre individuellen Ergebnisse mit Ihnen persönlich besprechen. Selbstverständlich erfahren Sie auch die allgemeinen Ergebnisse der Studie. 


## Wie kann ich mitmachen?
Für die Teilnahme mit Ihrem Kind schreiben Sie einfach eine E-Mail an [kinderlabor@plus.ac.at](mailto:kinderlabor@plus.ac.at?subject=Studienteilnahme %22Es rappelt in der Kiste%22&body=Liebes Kinderlabor-Team%2C%0D%0A%0D%0Amein Sohn%2Fmeine Tochter und ich w%C3%BCrden gerne an der Studie %22Es rappelt in der Kiste%22 teilnehmen.%0D%0AMein Kind hei%C3%9Ft %5BNAME DES KINDES%5D und ist %5BALTER DES KINDES%5D Monate alt.%0D%0A%0D%0ALiebe Gr%C3%BC%C3%9Fe%0D%0A%5BIHR NAME%5D) oder kontaktieren Sie uns telefonisch bzw. per WhatsApp ([0681 813 090 29](tel:+4368181309029)). Bei der Terminvereinbarung sind wir flexibel und passen uns Ihrem individuellen Tagesablauf an.
Haben Sie noch offene Fragen? Dann kontaktieren Sie uns ebenfalls, wir beantworten diese gerne. <br>

<br>

Wir freuen uns auf Ihren Besuch im Theory of Mind-Kinderlabor! 

Anna und Fiona 