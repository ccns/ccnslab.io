---
Title: Wie läuft eine Studie im ToM Kinderlabor ab?
Subtitle: Informationen zum ToM Kinderlabor
summary: Informationen zum ToM Kinderlabor
Order: 2
---
## Terminvereinbarung & Begrüßung
Wir suchen gemeinsam mit Ihnen einen Termin, der gut in Ihren individuellen Tagesablauf passt. Sie und Ihr Kind werden von uns am Universitätseingang abgeholt und in den Kinder-Laborraum begleitet.

## Elterninformation & Kennenlernen
Zu Beginn nehmen wir uns Zeit, um Ihnen alles genau zu erklären. Sie sind während des gesamten Besuches bei Ihrem Kind und können z. B. eine Pause vorschlagen, wenn Sie merken, dass Ihr Kind müde ist, gefüttert oder gewickelt werden muss.

Besonders wichtig ist es uns, dass sich die Kinder wohl fühlen und Vertrauen aufbauen können, bevor wir mit den Experimenten beginnen. Nur dann können Kinder auch zeigen was sie schon können!

Bei unseren Aufgaben gibt es kein “richtig” oder “falsch”, denn die Ergebnisse werden nicht individuell sondern immer im Gesamtzusammenhang betrachtet. Großen Wert legen wir auf kindgerechte Aufgaben sowie eine spielerische Atmosphäre, nur so können dieKinder uns auch zeigen was sie schon können.

## Studiendurchführung
Unsere Studien haben jeweils standardisierte Vorgehensweisen, die immer in spielerische Abläufe integriert sind.  Hier zum Beispiel beobachten wir, für welche von zwei Kisten sich das Kind entscheidet, wenn es der Versuchsleiterin helfen möchte.

## Abschluss
Am Ende des ca. 30minütigen Besuchs bekommt Ihr Kind eine Urkunde und ein kleines Dankeschön für die Teilnahme.