---
Title: Auditory Neuroscience Lab
Order: 1
---

{% import 'news.html' as news with context %}

How can we make sense of our acoustic environment, considering the fact that different sound sources in our complex environments (e.g. the classically cited “cocktail party") often activate the same receptors simultaneously? Only some information will be relevant for guiding our behaviour, while other information will be irrelevant or even distracting.

Next to a “faithful" transduction process and feedforward transmission of information, the listening-challenge can only be solved by appropriate deployment of top-down processes, such as attention or (anticipatory) prediction. The Auditory Neuroscience Group at the Salzburg Brain Dynamics lab is in particularly interested in neural processes linked to prediction, which could be derived e.g. by statistical regularities derived from previous input or by contextual cues (e.g. lip-movements accompanying speech). A particular focus of our group within this context is to shed light on the underinvestigated role corticofugal processes, which includes subcortical and even cochlear processes. We aim at linking these processes to higher-order cortical systems, that are more commonly investigated within cognitive neuroscience. For this purpose we are advancing simultaneous non-invasive neuro-cochlear measurements, which among others includes combination of measurement instruments (e.g. MEG and otoacoustic emissions) and innovative signal processing strategies applied to M/EEG data.

Our research has strong clinical implications, that we are currently pursuing in separate projects funded by the European Commission (Marie Curie Actions), FWF and FFG/MED-EL.

### Latest Publications

{% publication_cards tags=['auditory'], max_articles=9 %}

### Current and Past Funding
{% set funding = {'FWF': 'FWF_Logo.png', 'FFG': 'FFG_Logo_DE_RGB_1000px.png', 'ERC': 'ERC_logo.png', 'Land Salzburg': 'landsbg2015_4c_72dpi.png', 'DFG': 'dfg_logo_schriftzug_blau.jpg', 'Marie Curie Actions': 'marie_curie_actions_logo.jpg'} %}

{% funding funding, '/images/labs/auditory/funding_agencies/' %}

### Current and Past Industry Partners

{% funding {'Ababax': 'Ax_Logo_2.1 Regular_RGB.svg', 'WSA': 'logo_wsa.svg', 'MED-EL': 'MED-EL_RGB_Red.png'}, '/images/labs/auditory/funding_agencies/' %}

<!--
### News
{{ news.news(['auditory'], include_images=False) }} -->