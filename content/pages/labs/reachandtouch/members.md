---
Title: Team
Order: 2
---

{% from 'members.html' import members with context %}

## Current members

{{ members(positions=[['Full Professor'], ['Associate Professor'], ['Administrative Assistant', 'Office Administration'], ['Senior Scientist'], ['PostDoc'], ['PhD Student'], ['TA', 'BTA', 'RTA']]) }}

## Alumni

{{ members(positions=[['Alumni']], show_position=False) }}
