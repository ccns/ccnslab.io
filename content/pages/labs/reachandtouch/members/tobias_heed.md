---
Title: Tobias Heed
Date: 20.06.2022
Name: Tobias
Surname: Heed
Position: Full Professor
Tags: ccns, reachandtouch
Email: tobias.heed@plus.ac.at
Phone: ' +4366280445124'
home_url: https://www.plus.ac.at/psychology/ueber-uns/internal-organisation/division-of-cognition-development/reach-touch-lab/people/heed-tobias/?lang=en
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Photo: tobias_heed.jpg
CV: Heed_Tobias.pdf
Pure:
  id: 11868903
  types:
      article
      preprint
      shortreport
      review
      comment
  size: 100
---

I studied Business Administration at the Berufsakademie in Stuttgart and Psychology at the Philipps-University Marburg. After a PhD & Postdoc at the University of Hamburg's Biopsychology I established the Reach & Touch Lab as an Emmy Noether Junior Research Group at Hamburg's Psychology Department. In 2016, I moved the lab to Bielefeld University as Professor for Biopsychology & Cognitive Neuroscience, and in 2021 to the University of Salzburg's Centre for Cognitive Neuroscience as Professor for Cognitive Psychology.

I am fascinated by how we coordinate our bodies to interact with our environment, and how we relate the world to our body.
In my research, I start at a basic level and ask how the brain manages seemingly simple things like perceiving touch and performing arm reaches. My aim is to tease out how such simple processes lead to more complex phenomena such as a perception of self, an understanding of space, and the ability to merge information from the different senses into an integrated whole.
My work has focused on two aspects of this fascination with the body: our sense of touch, and movements with different body parts or, as we call them, effectors.

# Touch and the body in space
Our sense of touch is especially important in determining who we are: after all, the skin is the border between our body and the world. But because we have very versatile bodies, the information about where our body is and where one body part is relative to another, is constantly subject to change. Thus, although our body is somehow "always the same" with its anatomy and body parts, it is also "constantly different" because its configuration in space changes from moment to moment. One of my research foci is the interaction between these two sides of ourselves in space.

# Effectors
We don’t perceive things (like touch) just for their own sake. Instead, we perceive so that we can make decisions about which actions to take next. In many situations we could, in theory, use many ways to achieve our current goals.
Just think of opening your door after coming home from a shopping trip. Normally, you’ll use your dominant hand to press the door’s lever. But if you are carrying a shopping bag, you might use your other hand. Or, if you are carrying a big box with both hands, you might use your elbow, knee, or foot to open the door. How the brain is able to keep “in mind” these different options and choose between them is a second focus of my group’s work.

# Bringing it together
No movement occurs without creating tactile sensations. And when we want to move towards a touch we've just felt, we can't use the body part that was touched, but must choose a different body part to perform the reach. These are but two examples of how movement and somatosensation – our sense of touch and proprioception – are inseparably intertwined. I'd love to know how it all works. It's what drives my research and keeps me curious.