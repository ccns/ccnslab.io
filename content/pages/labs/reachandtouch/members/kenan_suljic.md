---
Title: Kenan Suljic
Date: 17.08.2022
Name: Kenan
Surname: Suljic
Position: Alumni
Tags: reachandtouch, bielefeld
Email: kenan.suljic@uni-bielefeld.de
Phone: +495211064486
home_url: https://www.uni-bielefeld.de/fakultaeten/psychologie/abteilung/arbeitseinheiten/14/team/wissensch-mitarbeit/suljic/
Affiliations:
    Bielefeld University
    Department of Psychology and Sports Science
Photo: kenan_suljic.jpg
---
