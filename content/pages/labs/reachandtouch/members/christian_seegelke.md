---
Title: Christian Seegelke
Date: 17.08.2022
Name: Christian
Surname: Seegelke
Position: Senior Scientist
Tags: ccns, reachandtouch
Email: christian.seegelke@plus.ac.at
Phone: ' +4366280445142'
home_url: https://www.plus.ac.at/psychology/ueber-uns/internal-organisation/division-of-cognition-development/reach-touch-lab/people/seegelke-christian/?lang=en
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Photo: christian_seegelke.jpg
Pure:
  id: 11868926
  types:
      article
      preprint
      shortreport
      review
      comment
  size: 100
---