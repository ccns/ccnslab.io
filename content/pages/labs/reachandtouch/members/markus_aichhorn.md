---
Title: Markus Aichhorn
Date: 29.06.2022
Name: Markus
Surname: Aichhorn
Position: Senior Scientist
Tags: ccns, reachandtouch
Email: markus.aichhorn@plus.ac.at
Phone: ' +4366280445125'
home_url: https://www.plus.ac.at/psychology/ueber-uns/internal-organisation/division-of-cognition-development/reach-touch-lab/people/aichhorn-markus/?lang=en
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Photo: markus_aichhorn.jpg
Pure: 2582196
---