---
Title: Marlou Perquin
Date: 17.08.2022
Name: Marlou
Surname: Perquin
Position: Alumni
Tags: reachandtouch, bielefeld
Email: marlou.perquin@uni-bielefeld.de
Phone: +495211064484
home_url: https://www.uni-bielefeld.de/fakultaeten/psychologie/abteilung/arbeitseinheiten/14/team/wissensch-mitarbeit/perquin/
Affiliations:
    Bielefeld University
    Department of Psychology and Sports Science
Photo: marlou_perquin.jpg
---
