---
Title: Carolin Schonard
Date: 17.08.2022
Name: Carolin
Surname: Schonard
Position: Alumni
Tags: reachandtouch, bielefeld
Email: carolin.schonard@uni-bielefeld.de
Phone: +495211064484
home_url: https://www.uni-bielefeld.de/fakultaeten/psychologie/abteilung/arbeitseinheiten/14/team/wissensch-mitarbeit/schonard/
Affiliations:
    Bielefeld University
    Department of Psychology and Sports Science
Photo: carolin_schonard.jpg
---
