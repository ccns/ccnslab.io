---
Title: Xaver Fuchs
Date: 17.08.2022
Name: Xaver
Surname: Fuchs
Position: Senior Scientist
Tags: ccns, reachandtouch
Phone: ' +4366280445168'
Email: xaver.fuchs@plus.ac.at
home_url: https://www.plus.ac.at/psychology/ueber-uns/internal-organisation/division-of-cognition-development/reach-touch-lab/people/fuchs-xaver/?lang=en
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Photo: xaver_fuchs.jpg
CV: Fuchs_Xaver.pdf
Pure:
  id: 11868954
  types:
      article
      preprint
      shortreport
      review
      comment
  size: 100
---