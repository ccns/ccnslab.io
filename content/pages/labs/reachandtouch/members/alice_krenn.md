---
Title: Alice Krenn
Date: 21.02.2024
Name: Alice
Surname: Krenn
Position: Office Administration
Tags: ccns, reachandtouch
Phone: +4366280445105
Email: alice.krenn@plus.ac.at
home_url: https://www.plus.ac.at/psychology/ueber-uns/internal-organisation/division-of-cognition-development/reach-touch-lab/people/krenn-alice/?lang=en
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Photo: alice_krenn.jpg
---