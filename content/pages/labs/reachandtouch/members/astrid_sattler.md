---
Title: Astrid Sattler
Date: 17.08.2022
Name: Astrid
Surname: Sattler
Position: Alumni
Tags: ccns, reachandtouch, salzburg, sexandgender
Phone: +4366280445105
Email: astrid.sattler@plus.ac.at
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---
