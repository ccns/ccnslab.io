---
Title: Juliane Schubert
Date: 13.05.2024
Name: Juliane
Surname: Schubert
Position: Senior Scientist
Tags: ccns, reachandtouch
Phone: ' +4366280445168'
Email: juliane.schubert@plus.ac.at
Photo: juliane_schubert.png
home_url: https://www.plus.ac.at/psychology/ueber-uns/internal-organisation/division-of-cognition-development/reach-touch-lab/people/schubert-juliane/?lang=en
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Pure: 5737394
---