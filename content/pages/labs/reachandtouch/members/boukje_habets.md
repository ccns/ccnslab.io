---
Title: Boukje Habets
Date: 20.06.2022
Name: Boukje
Surname: Habets
Position: Senior Scientist
Tags: ccns, reachandtouch
Phone: ' +4366280445168'
Email: boukje.habets@plus.ac.at
home_url: https://www.plus.ac.at/psychology/ueber-uns/internal-organisation/division-of-cognition-development/reach-touch-lab/people/habets-boukje/?lang=en
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Photo: boukje_habets.jpg
Pure: 11869326
---