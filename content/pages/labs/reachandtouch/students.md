---
Title: Students
Order: 3
---

[ please note: this page contains information for students of Psychology at the University of Salzburg and is, therefore, in German. ]

<br>
<br>

{% image_with_text image_file="images/labs/reachandtouch/students/student_BA_MA.JPG", image_caption="Image: Simon Haigermoser", image_side="left", hide_image_mobile=False %}
## Sie überlegen, eine BSc- oder MSc-Arbeit bei uns zu absolvieren?
[Hier](https://ccns.gitlab.io/reachandtouch/rtl-howtoexperiment/) finden Sie Informationen zu BSc- und MSc-Arbeiten in unserer Gruppe - über unser [allgemeines Vorgehen](https://ccns.gitlab.io/reachandtouch/rtl-howtoexperiment/) und [welche Themen](https://ccns.gitlab.io/reachandtouch/rtl-howtoexperiment/03-available-projects.html) derzeit bei uns bearbeitet werden und somit für BSc- und MSc-Arbeiten bei uns möglich sind.

## Sie sind bereits bei uns, planen Ihre Studie und bereiten Ihr Planungsreferat vor?
[Hier](https://ccns.gitlab.io/reachandtouch/rtl-howtoexperiment/02-questions-before-starting.html) finden Sie die Fragen, die Sie im Rahmen der Planung beantworten müssen. Bitte laden Sie sich das docx herunter (siehe roter Kasten oben auf der [Seite!](https://ccns.gitlab.io/reachandtouch/rtl-howtoexperiment/02-questions-before-starting.html)), um die Fragen dort auszufüllen.
{% endimage_with_text %}


{% image_with_text image_file="images/labs/reachandtouch/students/student_Data.JPG", image_caption="Image: Simon Haigermoser", image_side="left", hide_image_mobile=False %}
## Sie erheben (bald) Daten in unseren Labors?
Bitte lesen Sie sich unsere [Infos zur Nutzung unserer Labore](https://ccns.gitlab.io/reachandtouch/rtl_labs/) sorgfältig durch. Wir betreuen immer viele Studierende gleichzeitig - das macht ein wenig Orga und gegenseitige Rücksicht notwendig. Danke für Ihre aktive Mithilfe!

## Sie haben Ihre (ersten) Daten im Kasten und wollen zu analysieren beginnen?
Wir haben eine [ausführliche Dokumentation einer typischen Datenauswertung](https://ccns.gitlab.io/reachandtouch/rtl_RTut/) angefertigt, deren Code auch heruntergeladen und von Ihnen angepasst und verwendet werden kann.
{% endimage_with_text %}


## Feedback

Irgendwo finden sich Fehler auf unseren Info-Seiten? Es fehlt was? Man könnte es besser erklären? Bitte lassen Sie uns so was immer wissen, damit wir unsere Seiten kontinuierlich verbessern können. Kontaktieren Sie dazu bitte [Xaver Fuchs](https://ccns.plus.ac.at/labs/reachandtouch/members/xaver_fuchs/).




