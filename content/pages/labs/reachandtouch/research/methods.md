---
Title: Methods
Order: 4
---

{% image_with_text "images/labs/reachandtouch/methods/Mocap.jpeg", "Image: G. Fisher Mania", "right", "Motion tracking" %}
In the empirical measurement of motion, small infra-red (IR) light emitting diodes (LED) are attached to the body and illuminated. The light impulses are recorded by means of a camera system, allowing us to characterize body movements in 3D space. The analysis of movement parameters, such as movement time, end point, maximal velocity, or the time/ spatial location of an in-flight directional change, is informative as to how the brain plans movements in a variety of experimental settings.
{% endimage_with_text %}

{% image_with_text "images/labs/reachandtouch/methods/EEG.jpeg", "Image: G. Fisher Mania", "left", "EEG" %}
The electroencephalogram (EEG) is recorded at the scalp surface and measures the electrical activity of the brain. Analyzing EEG allows us to investigate brain processes with a high temporal resolution. A high temporal resolution is necessary to look at evoked and induced oscillatory activity during different sensory-motor tasks employed in our lab. For more information about EEG, [click here](https://www.youtube.com/playlist?list=PLOI_e1h0yeClmbpf9bxQdE9LyEKXcfwSr).
{% endimage_with_text %}

{% image_with_text "images/labs/reachandtouch/methods/MRT.png", "Image: Reach and touch lab", "right", "fMRI" %}
Functional magnetic resonance imaging (fMRI) is a non-invasive imaging method to measure blood flow changes in the brain. Researchers assume that blood level oxygenation is  a proxy for neuronal processing, allowing us to characterize which processes brain regions are involved in, their connectivity to other regions, and what kind of coding a region uses with high spatial resolution across the course of an experimental task.
{% endimage_with_text %}
