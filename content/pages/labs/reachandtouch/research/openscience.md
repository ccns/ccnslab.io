---
Title: Open Science
Order: 3
---

{% image_with_text image_file="images/labs/reachandtouch/landingpage/open_science.jpg", image_caption="Image: Simon Haigermoser", image_side="right", text_title="Open hypotheses, data, and code", hide_image_mobile=False %}
We support "open science”, preregsitration, preprints. Here you can find published code and data. Here is a list of recent articles and preprints that adhere to open science principles.
{% endimage_with_text %}


- **Martel, Fuchs, Trojan, Gockel, Habets & Heed, 2022, Cortex:<br>
Illusory tactile movement crosses arms and legs and is coded in external space**<br>
[Article](https://www.sciencedirect.com/science/article/pii/S0010945222000363?via%3Dihub)<br>
[Data and Code](https://osf.io/a8bgt/)

- **Schonard, Heed, Seegelke, 2022, bioRxiv:<br>
Allocation of visuospatial attention indexes evidence accumulation for reach decisions**<br>
[Preprint](https://www.biorxiv.org/content/10.1101/2022.05.06.490925v1)<br>
[Data and Code](https://osf.io/rxfjm/)

- **Seegelke, Schonard, Heed, 2021, Journal of Neurophysiology:<br>
Repetition effects in action selection reflect effector- but not hemisphere-specific coding**<br>
[Article](https://journals.physiology.org/doi/full/10.1152/jn.00326.2021)<br>
[Data and Code](https://osf.io/a8bgt/)

- **Maij, Seegelke, Medendorp, Heed, 2020, eLife:<br>
External location of touch is constructed post-hoc based on limb choice**<br>
[Article](https://elifesciences.org/articles/57804)<br>
[Data and Code](https://osf.io/v8m9j/)

- **Fuchs, Wulff & Heed, 2020, Journal of Experimental Psychology:<br>
Online sensory feedback during active search improves tactile localization**<br>
[Article](https://psycnet.apa.org/fulltext/2020-28134-001.html)<br>
[Data and Code](https://osf.io/v7hsj)<br>

- **Ossandón, König & Heed, 2020, Journal of Neuroscience:<br>
Spatially modulated alpha-band activity does not mediate tactile remapping and fast overt orienting behavior**<br>
[Preprint](https://doi.org/10.1101/576850)<br>
[Data and Code](https://osf.io/d7xc6)

- **Heed, Lajoie & Marigold, 2019, PLOS One:<br>
No effect of triple-pulse TMS medial to intraparietal sulcus on online correction for target perturbations during goal-directed hand and foot reaches**<br>
[Article](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0223986)<br>
[Data](https://osf.io/h3jym/)

- **Martel et al., Preprint:<br>
Protracted development of visuo-proprioceptive integration for uni-and bimanual motor coordination**<br>
[Preprint](https://doi.org/10.1101/601435)<br>
[Data and Code](https://osf.io/x9u27/)

- **Badde, Röder & Heed, 2019, Current Biology:<br>
Feeling a Touch to the Hand on the Foot**<br>
[Article](https://doi.org/10.1016/j.cub.2019.02.060)<br>
[Data and Code](https://github.com/StephBadde/HaendeFuessePhantomErrors)  

- **Brandes, Rezvani & Heed, 2017, Scientific Reports:<br>
Abstract spatial, but not body-related, visual information guides bimanual coordination**<br>
[Article](https://doi.org/10.1038/s41598-017-16860-x)<br>
[Data and Code](https://osf.io/g8jrt/)

- **Schubert et al., 2017, PLOS One:<br>
Task demands affect spatial reference frame weighting during tactile localization in sighted and congenitally blind adults**<br>
[Article](https://doi.org/10.1371/journal.pone.0189067)<br>
[Data and Code](https://osf.io/ykqhd/)

- **Heed et al., 2016, PLOS One:<br>
Disentangling the External Reference Frames Relevant to Tactile Localization**<br>
[Article](https://doi.org/10.1371/journal.pone.0189067)<br>
[Data and Code](https://osf.io/3zq75/)
