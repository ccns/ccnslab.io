---
Title: Publications
Order: 1
---

{% from 'publications.html' import publication_list with context %}

This page lists papers that came out of the Reach & Touch Lab. For papers that originated from our group members' previous positions and individual collaborations, please refer [to individual Team pages](labs/reachandtouch/members/index.html).

{% image_with_text image_file="images/labs/reachandtouch/landingpage/publications.jpg", image_caption="Image: Simon Haigermoser", image_side="left", text_title="To place our work in context...", hide_image_mobile=False %}
...take a look at these review papers, which place our work in the perspective of other research:

- **[The macaque ventral intraparietal area has expanded into three homologue human parietal areas](https://www.sciencedirect.com/science/article/pii/S0301008221001994)**<br>
(Foster, Sheng, et. al, 2022, Progress in Neurobiology, **open access**)

- **[State estimation in posterior parietal cortex: Distinct poles of environmental and bodily states](https://www.sciencedirect.com/science/article/pii/S0301008219301145)**<br>
(Medendorp & Heed, 2019, Progress in Neurobiology, **open access**)

- **[Towards explaining spatial touch perception: weighted integration of multiple location codes](http://www.tandfonline.com/doi/full/10.1080/02643294.2016.1168791)**<br>
(Badde & Heed, 2016, Cognitive Neuropsychology, **open access**)

- **[Tactile remapping: from coordinate transformation to integration in sensorimotor processing](http://www.sciencedirect.com/science/article/pii/S1364661315000492)**<br>
(Heed et al., 2015, Trends in Cognitive Sciences)

- **[Using time to investigate space: a review of tactile temporal order judgments as a window onto spatial processing in touch](http://www.frontiersin.org/Journal/10.3389/fpsyg.2014.00076/abstract)**<br>
(Heed & Azañón, 2014, Frontiers in Psychology, **open access**)
{% endimage_with_text %}



{{ publication_list('reachandtouch') }}
