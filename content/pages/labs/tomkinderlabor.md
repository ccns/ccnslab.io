---
Title: Theory of Mind Kinderlabor
subtitle:
short_title: ToM Kinderlabor
summary:
order: 7
---

{% import 'news.html' as news with context %}



## Über das Theory of Mind Kinderlabor
Haben Sie sich auch schon manchmal gefragt, wie kleine Kinder denken? Wie verstehen und erklären sie sich das, was sie beobachten? Tun sie das so wie wir Erwachsene, oder haben sie ganz andere Denkweisen?

Mit solchen Fragen beschäftigen wir uns in unserer entwicklungspsychologischen Forschung am Theory of Mind Kinderlabor.

Theory of Mind ist eine Bezeichnung für die  menschliche Fähigkeit, geistige Zustände bei anderen Menschen zu erkennen und einschätzen zu können. Diese Fähigkeit ist wichtig, um in sozialen Situationen die Sichtweise anderer Menschen nachvollziehen zu können.

Um „Theory of Mind“-Fähigkeiten bei kleinen Kindern zu erfassen, haben wir Spielsituationen entwickelt, bei denen wir die Reaktionen der Kinder beobachten, um daraus Rückschlüsse über ihr Verständnis dieser Situationen ziehen zu können.

Die Durchführung findet zum einen direkt bei uns an der Naturwissenschaftlichen Fakultät der Universität Salzburg in einem dafür eingerichteten Kinderlabor statt. Zum anderen führen wir Studien auch direkt in den Kinderbetreuungs- & Freizeiteinrichtungen durch.

**[Was ist "Theory of Mind"?]({filename}tomkinderlabor/was_ist_tom.md)**<br>
**[Wie läuft eine Studie im ToM-Kinderlabor ab?]({filename}tomkinderlabor/ablauf.md)**<br>
**[Aktuelle Projekte]({filename}tomkinderlabor/aktuelle_projekte.md)**<br>
**[Weitere Informationen]({filename}tomkinderlabor/informationen.md)**<br>
**[Kontakt und Anmeldung]({filename}tomkinderlabor/anmeldung.md)**<br>