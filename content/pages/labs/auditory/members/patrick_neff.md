---
Title: Patrick Neff
Date: 19.01.2023
Name: Patrick
Surname: Neff
Position: Alumnus
Tags: ccns, auditory
Email: patrick.neff@plus.ac.at
Photo: patrick_neff.jpg
---

## Research Interests
* Neuroscience and -modulation of tinnitus
* Auditory phantoms
* Digital sound synthesis
* Mobile systems

## Selected Publications
{% publication_paragraph "10.1177/2331216519833841", "Patrick Neff" %}
{% publication_paragraph "10.1016/j.neulet.2018.11.008", "Patrick Neff" %}
{% publication_paragraph "10.3389/fnagi.2017.00130", "Patrick Neff" %}
{% publication_paragraph "10.1016/j.brainres.2017.03.007", "Patrick Neff" %}
{% publication_paragraph "10.1016/j.heares.2016.08.016", "Patrick Neff" %}

