---
Title: Anne Hauswald
Date: 20.06.2022
Name: Anne
Surname: Hauswald
Position: Senior Scientist
Tags: ccns, auditory
Email: anne.hauswald@plus.ac.at
Phone: ' +4366280445127'
Photo: anne_hauswald.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Pure: 2589887
---

## Research Interests:
* Visual speech, audio-visual speech
* Brain networks states, connectivity, and oscillatory states
* Characterization of resting state networks using graph theory
* Link between brain states and memory, attention, meditation