---
Title: Sabine Stummvoll
Date: 18.01.2023
Name: Sabine
Surname: Stummvoll
Position: Secretary
Tags: ccns, auditory
Email: sabine.stummvoll@plus.ac.at
Phone: 0662 8044 5100
Photo: sabine_stummvoll.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---