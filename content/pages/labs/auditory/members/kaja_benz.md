---
Title: Kaja Benz
Date: 18.01.2023
Name: Kaja
Surname: Benz
Position: PhD Student
Tags: ccns, auditory
Email: kajarosa.benz@plus.ac.at
Photo: kaja_benz.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Pure: 2574630
---

## Research Interests:
* Audiovisual speech perception
* Hidden hearing loss
* Hyperalignment in MEG