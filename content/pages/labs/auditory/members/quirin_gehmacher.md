---
Title: Quirin Gehmacher
Date: 18.01.2023
Name: Quirin
Surname: Gehmacher
Position: Alumnus
Tags: ccns, auditory
Email: quirin.gehmacher@plus.ac.at
Photo: quirin_gehmacher.jpg
Pure: 5293583
---

## Research Interests:
* Auditory Neuroscience
* Cochlear implants