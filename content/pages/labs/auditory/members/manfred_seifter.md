---
Title: Manfred Seifter
Date: 18.01.2023
Name: Manfred
Surname: Seifter
Position: Technical Assistent
Tags: ccns, auditory
Email: manfred.seifter@plus.ac.at
Phone: 0662 8044 5185
Photo: manfred_seifter.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---

## Tasks:
* Day-to-day business in the MEG Lab
* Lab administration
* Assistance in MEG testings and conducting MEG testings
* Maintenance and repairs
* Planning and assignment of lab capacities (Calendar…)
* Material management
* Performing basic MRI scans
