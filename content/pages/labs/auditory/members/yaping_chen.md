---
Title: Ya-Ping Chen
Date: 18.01.2023
Name: Ya-Ping
Surname: Chen
Position: PostDoc
Tags: ccns, auditory
Email: ya-ping.chen@plus.ac.at
Photo: yaping_chen.jpg
Pure: 2581527
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---

## Research Interests:
* Visual facilitation in speech perception
* Alpha oscillations in auditory and vision perception
* Hearing rehabilitation and vision rehabilitation
