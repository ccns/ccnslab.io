---
Title: Chandra Haider
Date: 18.01.2023
Name: Chandra
Surname: Haider
Position: Alumnus
Tags: ccns, auditory
Email: chandraleon.haider@plus.ac.at
Photo: chandra_haider.png
Pure: 10071848
---

## Research Interests:
* Visual facilitation in speech perception
* Effects of face mask on speech processing and perception