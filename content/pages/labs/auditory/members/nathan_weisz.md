---
Title: Nathan Weisz
Date: 20.06.2022
Name: Nathan
Surname: Weisz
Position: Full Professor
Tags: ccns, auditory, neurogram
Email: nathan.weisz@plus.ac.at
Phone: +4366280445120
Photo: nathan_weisz.jpg
CV: Weisz_Nathan.pdf
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Pure:
    id: 2579471
    types: ["article", "shortreport", "review", "comment"]
---

## Research Interests
* Fluctuating network states as predispositions of conscious perception
* Interaction between brain oscillations and network-level states
* Neural mechanisms of tinnitus
* Online effects of neurostimulation
