---
Title: Fabian Schmidt
Date: 18.01.2023
Name: Fabian
Surname: Schmidt
Position: PostDoc
Tags: ccns, auditory, neurogram
Email: fabian.schmidt@plus.ac.at
Photo: fabian_schmidt.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Pure: 2592805
---

## Research Interests:
* Early auditory evoked activity
* Auditory attention and perception