---
Title: Thomas Hartmann
Date: 20.06.2022
Name: Thomas
Surname: Hartmann
Position: Senior Scientist
Tags: ccns, auditory
Email: thomas.hartmann@plus.ac.at
Phone: +4366280445109
Photo: thomas_hartmann.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Pure: 2588602
---

## Research Interests
* Real-Time acquisition and processing of EEG and MEG signals (see also my work on ConSole)
* State dependent (r)TMS
* Ongoing Brain Oscillations
* Digital Signal Processing of EEG,  MEG and intracranial signals
* Neuroscience of Tinnitus

## Open Source Software I maintain
* [O_PTB: Easier Experiments with the Psychophysics Toolbox](https://o-ptb.readthedocs.io/)
* [obob_ownft: Our FieldTrip based analysis environment](https://gitlab.com/obob/obob_ownft)
* [Access the Labjack in Matlab on Linux](https://gitlab.com/thht/lj_python_matlab)
* [pymatreader: Read Matlab files in Python](https://gitlab.com/obob/pymatreader)
* [obob_subjectdb: Keep track of participants and metadata](https://gitlab.com/obob/obob_subjectdb)
* [django_auto_url: Automatic Urls fro Django](django_auto_url: Automatic Urls fro Django)
* [django-auto-webassets: Better handling of webassets in Django](https://gitlab.com/thht_django/django-auto-webassets)

## Open Source Software I contribute to
* [FieldTrip](http://www.fieldtriptoolbox.org/)
* [MNE Python](https://mne-tools.github.io/)

## My personal website with tutorials
[https://thht.gitlab.io/](https://thht.gitlab.io/)