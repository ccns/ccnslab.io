---
Title: Lisa Reisinger
Date: 18.01.2023
Name: Lisa
Surname: Reisinger
Position: Alumnus
Tags: ccns, auditory
Email: lisa.reisinger@plus.ac.at
Photo: lisa_reisinger.jpg
Pure: 10927259
---

## Research Interests:
* Tinnitus
* Hearing loss