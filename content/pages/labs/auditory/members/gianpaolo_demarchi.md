---
Title: Gianpaolo Demarchi
Date: 20.06.2022
Name: Gianpaolo
Surname: Demarchi
Position: Assistant Professor
Tags: ccns, auditory
Email: gianpaolo.demarchi@plus.ac.at
Phone: ' +4366280445135'
Photo: gianpaolo_demarchi.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Pure: 2588374
---

## Research Interests
* Signal processing and analysis methods
* Novel applications of the MEG technique (e.g. combined MEG-tES, MEG-CI, MEG-iEEG, etc.)
* Ongoing Brain Oscillations
* Sensory processing