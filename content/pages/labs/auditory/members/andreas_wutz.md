---
Title: Andreas Wutz
Date: 20.06.2022
Name: Andreas
Surname: Wutz
Position: PostDoc
Tags: ccns, auditory
Email: andreas.wutz@plus.ac.at
Photo: andreas_wutz.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
Pure: 2579112
---

## Research Interests
* Neural network communication through oscillations 
* Neural oscillations as substrates of perception, attention, working memory and thought
* Temporal dynamics of perception and coginition
* Mental capacity limits
* Abstraction by categories and concepts