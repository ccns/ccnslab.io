---
Title: Nina Süß
Date: 18.01.2023
Name: Nina
Surname: Süß
Position: Alumnus
Tags: ccns, auditory
Email: nina.suess@plus.ac.at
Photo: nina_suess.jpg
Pure: 4732740
---

## Research Interests:
* Attentional modulation of ongoing brain oscillations in near threshold paradigms
* Audiovisual cortical networks in deaf and hard hearing individuals
* Hearing rehabilitation in CI patients
