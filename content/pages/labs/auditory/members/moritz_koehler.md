---
Title: Moritz Köhler
Date: 18.01.2023
Name: Moritz
Surname: Köhler
Position: Alumnus
Tags: ccns, auditory
Email: moritz.koehler@plus.ac.at
Photo: moritz_koehler.jpg
Pure: 2589934
---

## Research Interests:
* Attentional modulation of otoacoustic measures
* Attentional modulation of brain oscillations across sensory modalities
* Memory consolidation during sleep
