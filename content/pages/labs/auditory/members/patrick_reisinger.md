---
Title: Patrick Reisinger
Date: 18.01.2023
Name: Patrick
Surname: Reisinger
Position: Alumnus
Tags: ccns, auditory
Email: patrick.reisinger@plus.ac.at
Photo: patrick_reisinger.jpg
Pure: 5626891
---

## Research Interests:
* Auditory nerve recordings using cochlear implants (Project: [Smart cochlear implants](https://projekte.ffg.at/projekt/3186928))
* Listening effort, auditory attention
* Neural (audiovisual) speech tracking (Temporal response functions)