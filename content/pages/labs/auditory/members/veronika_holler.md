---
Title: Veronika Holler
Date: 14.11.2024
Name: Veronika
Surname: Holler
Position: PhD Student
Tags: ccns, auditory
Email: veronikamarina.holler@plus.ac.at
Photo: veronika_holler.jpg
Affiliations:
    PLUS Faculty of Natural and Life Sciences
    Department of Psychology
---
