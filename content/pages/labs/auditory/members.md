---
Title: Team
Order: 2
---

{% from 'members.html' import members with context %}

{{ members(positions=[['Full Professor'], ['Associate Professor'], ['Assistant Professor'], ['Senior Scientist'], ['PostDoc'], ['PhD Student'], ['Technical Assistent', 'Secretary']]) }}

### Alumni

{{ members(positions=[['Alumnus']], show_position=False) }}