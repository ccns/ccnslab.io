---
Title: Neuro-AI guided objective hearing assessment and hearing loss compensation
Short_Title: Neurogram
Order: 1
---

{% from 'members.html' import members with context %}

## Abstract

Hearing loss is a widespread issue that impacts millions of people globally, often leading to difficulties in communication and contributing to cognitive decline. In fact, untreated hearing loss in middle age is recognized as the largest modifiable risk factor for dementia. Yet, the most commonly used hearing test, the Pure Tone Audiogram (PTA), has remained largely unchanged for over 100 years. PTA can detect faint artificial tones but provides little information on how we process louder, everyday sounds. This limitation often leads to unsatisfactory experiences with hearing aids. 

In collaboration with artificial intelligence (AI) experts from France, and collaborators in Denmark, and the UK, we are aiming to change that. Together we have developed an innovative approach called the "Neurogram", which goes beyond traditional methods to better assess how the brain processes a wider range of sounds. Unlike PTA, the Neurogram measures brain activity while participants listen to real-world sounds, such as audiobooks. By reconstructing how different sound frequencies are processed, the Neurogram offers a more detailed and accurate picture of hearing. 

What sets this project apart is its use of AI. The researchers will employ deep learning techniques to predict how changes in sound, such as those made by a hearing aid, impact the Neurogram. This AI-based approach will help tailor hearing aids to provide a clearer, more natural listening experience for users. Another key innovation is the use of EEG, a more widely accessible method of recording brain activity. While previous versions of the Neurogram relied on MEG, a less common technology, the team will now explore how EEG can make the method available to a broader audience. 

This project could significantly change the way hearing loss is assessed and treated, providing a more objective way to optimize sound processing in hearing aids. The Neurogram has the potential to reshape hearing aid technology and improve the quality of life for millions of people with hearing loss.

## Team
{{ members(positions=[['Full Professor', 'Principal Investigator'], ['PostDoc'], ['PhD Student']], tags=['neurogram'], show_position=False) }}


 