---
Title: Eating Behavior Lab
Order: 2
---

<center> <h1>Welcome to our website!</h1> </center>

We investigate basic appetitive mechanisms to understand overeating and food craving and also develop respective interventions.

![]({static}/images/labs/eat/landing_page/eatlogo.jpg)

#Main research topics#
Assessment of and intervention on eating behavior and related health behavior such as...</br>
<ul>
<li>eating disorder symptoms (i.e., binge eating, restriction)</li>
<li>food related approach-avoidance tendencies</li>
<li>eating behaviors/styles (i.e., emotional eating, stress eating, restrictive eating)</li>
<li>intention behavior gaps</li>
<li>food cue reactivity and food craving</li>
<li>physical activity</li>
</ul>
...in healthy, overweight and eating disordered individuals. We investigate these topics with a range of different methods including smartphone-based ambulatory assessments and interventions, computerized behavioral tasks (i.e., approach avoidance task, idiosyncratic emotional food task, food decision task), tracking of physical activity, and psychophysiological measures including EEG and fMRI.

#Further information#
You can currently find further information about our team, research, publications and ressources [here](https://www.eat.sbg.ac.at).