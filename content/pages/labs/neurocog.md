---
Title: Neurocognition Lab
Order: 3
---

# Research clusters in which we are involved

**Cognitive Neuroscience and Brain Disorders.** In our aging society the prevalence of brain lesions as a result of strokes is on the rise. Strokes often lead to functional disorders, such as e.g. visual field loss. In one of our projects we aim to improve the diagnosis (i.e., perimetry) and the rehabilitation of visual field loss and/or visual neglect by means of high-end, state-of-the-art neuroscientific methods, such as eye-tracking assisted visual field diagnosis and high-precision visual stimulation for therapeutic intervention. Furthermore, we aim to improve the effectiveness and the comfort of therapeutic measures with virtual and augmented reality.   

Other research topics include i.) Transdiagnostic and dimensional approaches to functional and structural brain abnormalities in mental disorders, ii.) Precision mapping of individual brain connectivity abnormalities in mental disorders, iii.) Social cognition and emotion processing in epilepsy and other brain disorders and iv.) Long-term prognosis in disorders of consciousness with functional and structural brain markers.

**Developmental aspects of cognition.** Our research covers several developmental stages, in particular school-aged children and adolescents. Exemplary research topics include i) parent-child attachment, ii) cognitive development and theory of mind, and iii) developmental learning disorders.

**Mental & Physical Fitness.** Our research covers diverse aspects of mental and physical performance and well-being in athletes. Exemplary research topics include i) neural correlates of football expertise, ii) impact of heading a football on the brain, iii) sleep and recovery in athletes, and iv) mental performance enhancement through Virtual Reality training.

**Neuroscience and the Law.** Researchers in the Neurocognition Lab are interested in Neuroscience and the Law, along with colleagues in the Faculty of Law. We have recently attracted two externally funded projects, namely the Austrian NeuroCloud and the Digital Neuroscience Initiative. Research questions that require such interdisciplinary collaboration include the topic of privacy and neuroscience data the topics responsibility, liability, and mind as well as the connection between (impaired) social cognition and legal causation.

# Method Units
The Neurocognition Lab is responsible for the operation of the Neuroimaging & Brain Stimulation Unit, the Eye Tracking and Mental Chronometry Unit as well as the High Performance Computing Unit.

# Research Projects …
If you are interested in our research regarding visual field loss, check the [Advanced Perimetry project](https://ccns.plus.ac.at/groups/neurocog/research/projects/). If you are interested in Neuroscience & the Law, have a look at the [Digital Neuroscience Initiative](https://ccns.plus.ac.at/groups/neurocog/research/projects/). Check out the [Austrian NeuroCloud](https://ccns.plus.ac.at/groups/neurocog/research/projects/) project, if you are interested in open and reproducible neuroscience and high performance computing.

