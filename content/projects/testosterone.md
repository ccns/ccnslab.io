---
Title: Testosterone 
Date: 23.09.2022
Responsible: Belinda
Image: images/projects/testosterone2.png
Tags: sexandgender
---

Contrary to common beliefs, testosterone is not stable in males, but fluctuates in response to various stimuli. In our ongoing studies we investigate whether testosterone fluctuations in males relate to changes in the brain, cognition or mood.
