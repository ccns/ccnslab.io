---
Title: Advanced Perimetry
Date: 07.10.2022
Responsible: Florian
Image: images/projects/perimetry.jpg
Tags: neurokog
---

Visual field disorders are a common cause of strokes and traumatic brain injury, and limit people's ability to cope with everyday life (e.g. driving). In this project, we develop and evaluate an advanced method of perimetry using an eye movement camera that allows for more reliable and accurate measurements. This will enable us to subsequently assess the effectiveness of interventions.
