---
Title: AIDA Path
Date: 07.10.2022
Responsible: Florian
Image: images/projects/AIDA.png
Tags: neurokog
---

"AIDA-PATH" is a cooperation project between the German software company medicalvalues, the IDA Lab Salzburg and the CCNS and deals with the development of automated diagnostic pathways for mental and physical illnesses. For this purpose, the CCNS is developing an ontology-based approach to support a valid and selective diagnosis of mental illnesses, taking into account the currently valid guidelines (e.g. DSM-V).
