---
Title: Menstrual Cycle 
Date: 23.09.2022
Responsible: Belinda
Image: images/projects/menstrualcycle_free.png
Tags: sexandgender
---

Current results show that cognition hardly changes along the menstrual cycle. However, many brain connections respond to sex hormone fluctuations along the menstrual cycle. Our current menstrual cycle studies focus on this remarkable adaptive capacity of the female brain along the menstrual cycle.
