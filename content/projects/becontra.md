---
Title:  
Date: 23.09.2022
Responsible: Belinda
Image: images/projects/becontra.png
Tags: sexandgender
---

[**B**rain **E**ffects of Hormonal **Contra**ceptives](https://www.europeandissemination.eu/becontra/11695)

This project is interested in how birth control pills affect the female brain. We seek to identify predictors of psychological side effects in order to provide women with the necessary information for informed contraceptive choices.
