---
Title: Sex Hormones and Visual Processing
Date: 23.09.2022
Responsible: Belinda
Image: images/projects/visualproc2.png
Tags: sexandgender
---

Sex differences in cognitive tasks can be attributed to different processing styles, which are related to the selection of visual information for further processing. In a series of experiments we investigate how sex hormones relate to visual processing using neuroimaging and eye-tracking.
