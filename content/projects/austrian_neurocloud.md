---
Title: Austrian Neurocloud 
Date: 07.10.2022
Responsible: Florian
Image: images/projects/ANC.png
Tags: neurokog
---

Austrian NeuroCloud (ANC) aims at integrating Austria's scientific and technological infrastructure, including in particular its large-scale facilities and instrumentations, by building a centralized hub for standardised storage, administration and analysis of research data in the field of Cognitive Neuroscience. 
