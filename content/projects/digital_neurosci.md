---
Title: Digital Neuroscience Initiative
Date: 07.10.2022
Responsible: Florian
Image: images/projects/DNI.png
Tags: neurokog
---


The Digital NeuroScience Initiative seeks to provide the legal basis for the digitisation of human data and the development of innovative tools for the efficient management of Big Human Neurodata. This requires an interdisciplinary project partnership of cognitive neuroscience with law and computer science.
