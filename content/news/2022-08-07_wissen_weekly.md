---
Title: Podcast on Birth Control Pills
Date: 07.08.2022
Summary: Interview with Belinda Pletzer on the side effects of hormonal contraceptives for Wissen Weekly. Listen [here](https://open.spotify.com/episode/5AJMSlriuTT5IDu4MFKGMi?si=ffb9cf89594848c4&nd=1)
Image: images/news/wissen_weekly.jpg
Tags: sexandgender
external_url: https://open.spotify.com/episode/5AJMSlriuTT5IDu4MFKGMi?si=ffb9cf89594848c4&nd=1
---
