---
Title: Feature on the fe-male brain
Date: 13.09.2022
Summary: Interview with Belinda Pletzer on the discourse about sex differences in the brain. Listen [here](https://www.hoerspielundfeature.de/feature-fe-male-brain-102.html)
Image: images/news/fe-male_brain3.jpg
Tags: ccns, sexandgender
external_url: https://www.hoerspielundfeature.de/feature-fe-male-brain-102.html
---
