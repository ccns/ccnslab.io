---
Title: Dissertation Award for Michael Leitner
Date: 18.10.2022
Summary: ...awarded by the Chamber of Labour (Arbeiterkammer) for his dissertation project "Eye Tracking Perimetry and Virtual Reality Rehabilitation", supervised by Stefan Hawelka. 
Tags: ccns
---
Michael Leitner of the Salzburg University's Psychology Department has been awarded by the Chamber of Labour (Arbeiterkammer) for his dissertation project "Eye Tracking Perimetry and Virtual Reality Rehabilitation". The dissertation was supervised by Stefan Hawelka.
Congratulations!

