---
Title: Tactile training facilitates infants' ability to reach to targets on the body
Date: 01-19-2023
Summary: New paper from a true multilab collab headed by Eszter Somogyi (Portsmouth, UK)
Image: images/news/2023-01-19_rtl_paper_somogyi.jpg
Tags: reachandtouch
external_url: https://srcd.onlinelibrary.wiley.com/doi/10.1111/cdev.13891
---
