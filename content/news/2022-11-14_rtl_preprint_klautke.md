---
Title: Dynamic spatial coding in parietal cortex mediates hand movement planning towards touch
Date: 11-14-2022
Summary: New preprint by former lab member Janina Klautke and current lab member Celia Foster on bioRxiv
Image: images/news/2022-11-14_rtl_news_preprint_klautke.jpg
Tags: reachandtouch
external_url: https://www.biorxiv.org/content/10.1101/2022.11.12.516245v1
---

