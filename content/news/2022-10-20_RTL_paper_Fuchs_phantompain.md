---
Title: Prevalence of phantom phenomena in congenital and early-life amputees
Date: 10-20-2022
Summary: New paper co-authored by Xaver Fuchs – work with his previous lab – in Journal of Pain
Tags: reachandtouch
Image: images/news/2022-10-20_rtl_news_fuchs.jpg
external_url: https://doi.org/10.1016/j.jpain.2022.10.010
---
