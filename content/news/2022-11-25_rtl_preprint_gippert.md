---
Title: Prior movement of one arm facilitates motor adaptation in the other
Date: 11-25-2022
Summary: New preprint by former lab member Magdalena Gippert, co-authored by Tobias Heed, on bioRxiv
Image: images/news/2022-11-25 rtl preprint gippert.jpg
Tags: reachandtouch
external_url: https://www.biorxiv.org/content/10.1101/2022.11.22.517483v1
---


