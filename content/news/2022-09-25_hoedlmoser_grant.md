---
Title: FWF/ANR grant for Kerstin Hödlmoser
Date: 25.09.2022
Summary: Prof. Kerstin Hödlmoser received funding  in the FWF/ANR Austrian-French bilateral funding scheme for her proposal "Effects of sleep on motor sequence learning by motor imagery".
Tags: ccns
---

Prof. Kerstin Hödlmoser has acquired funding in the bilateral Austrian-French (FWF/ANR) funding scheme together with French collaborator Arnaud Saimpont (University of Lyon). The grant "Effects of sleep on motor sequence learning by motor imagery in young and older adults - acronyme Sleep, Motor Imagery, Learning, EEG, Sequence - SMILES) asks whether acquisition of a fine and a gross motor task is supported by motor imagery training differently in young and older adults. It investigates several EEG-related measures to answer this question. 

The study aims to test 120 young adults (20-25 yrs old) and 120 older adiults (65-80 years old).
