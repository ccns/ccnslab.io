---
Title: FWF grant for Manuel Schabus & opportunity to participate
Date: 13-02-2025
Summary: New FWF project at the Laboratory for Sleep and Consciousness Research & New sleep study on digital insomnia therapy - "Endlich wieder besser schlafen?".
Tags: ccns
---

<b>New FWF project at the Laboratory for Sleep and Consciousness Research</b>

After several attempts, we were finally successful in 12/2024 and received the FWF funding approval for our large-scale 4-year project “The Virtual Sleep Lab” (VSL).
The VSL is an innovative digital program for the treatment of insomnia (©sleep²) based on cognitive behavioral therapy for insomnia (CBT-I). VSL provides an easily accessible, science-based therapy for people with sleep disorders and combines daily feedback with objective measurements such as polysomnography and heart rate variability. This enables users to actively track and improve their sleep behavior. A randomized controlled trial (RCT) is investigating the effectiveness of VSL compared to conventional CBT-I groups and a control group to evaluate digital interventions as a cost-effective, scalable alternative to face-to-face therapy. With approximately 30% of the population suffering from clinical sleep problems, VSL has the potential to be an effective, widely accessible treatment option.




{% image_with_text image_file="images/labs/sleep/2025-02-12_news_Flyer_VSL_2025.png", image_caption="Image: Flyer", image_side="right", hide_image_mobile=False %}
<b>Endlich wieder besser schlafen? - New sleep study on digital insomnia therapy</b>

How effective is digital therapy in the treatment of insomnia? What is the significance of sleep tracking, i.e. sleep measurement with smart devices? Who benefits more from personal and who from digital sleep therapy? 
Under the direction of Univ.-Prof. Dr. Manuel Schabus and Alexandra Hinterberger, M.Sc., a new study will start in April at the sleep laboratory of the University of Salzburg, which will deal with precisely these questions. Anyone aged between 30 and 65 from the Salzburg area with sleep problems who is interested in taking part in the study is welcome to contact schlaflabor.studie@plus.ac.at.
{% endimage_with_text %}

 