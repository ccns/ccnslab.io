---
Title: Rescaling perceptual hand maps by visual-tactile recalibration
Date: 10-02-2024
Summary: New paper by Xaver Fuchs and Tobias Heed in EJN
Image: images/news/2024-10-02_rtl_paper_fuchs.jpg
Tags: reachandtouch
external_url: https://onlinelibrary.wiley.com/doi/10.1111/ejn.16571
---
