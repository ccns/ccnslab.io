---
Title: Illusory tactile movement crosses arms and legs and is coded in external space
Date: 04-01-2022
Summary: New paper by shared first authors Marie Martel & Xaver Fuchs in Cortex
Image: images/news/2022-04-01_rtl_news_martel_newsize.jpg
Tags: reachandtouch
external_url: https://www.sciencedirect.com/science/article/pii/S0010945222000363?via%3Dihub
---

The famous cutaneous rabbit illusion, where touch is perceived in a not stimulated spot between two touched locatations is based in external space. This Our suggests that prior experience of touch in space critically shapes tactile spatial perception and illusions beyond anatomical organization.

Read paper [here](https://www.sciencedirect.com/science/article/pii/S0010945222000363?via%3Dihub)
