---
Title: Exploring the response code in a compatibility effect between physical size and left/right responses
Date: 11-25-2022
Summary: New preprint by Christian Seegelke and collaborators on psyarchiv
Image: images/news/2022-11-25 rtl preprint seegelke.jpg
Tags: reachandtouch
external_url: https://psyarxiv.com/trf9h/
---


