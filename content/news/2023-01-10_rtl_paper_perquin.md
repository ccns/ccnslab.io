---
Title: Temporal Structure in Sensorimotor Variability - A Stable Trait, But What For?
Date: 01-10-2023
Summary: New paper by Marlou Perquin, together with collaborators, in Computatioonal Brain & Behavior
Image: images/news/2023-01-10_rtl_paper_perquin.jpg
Tags: reachandtouch
external_url: https://link.springer.com/article/10.1007/s42113-022-00162-1
---
