---
Title: New Preprint published
Date: 13.08.2022
Summary: Brain connectivity changes in oral contraceptive users vs. placebo. Read [here](https://www.medrxiv.org/content/10.1101/2022.08.11.22278664v2)
Image: images/news/coc_preprint.jpg
Tags: sexandgender
external_url: https://www.medrxiv.org/content/10.1101/2022.08.11.22278664v2
---
