---
Title: Variance (un)explained – Experimental conditions and temporal dependencies explain similarly small proportions of reaction time variability in perceptual and cognitive tasks
Date: 01-09-2023
Summary: New preprint by Marlou Perquin and collaborator Christoph Kayser on biorXiv
Image: images/news/2023-01-08_rtl_preprint_perquin.jpg
Tags: reachandtouch
external_url: https://www.biorxiv.org/content/10.1101/2022.12.22.521656v1
---
