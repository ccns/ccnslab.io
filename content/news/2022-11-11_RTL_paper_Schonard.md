---
Title: Allocation of visuospatial attention indexes evidence accumulation for reach decisions
Date: 11-11-2022
Summary: New paper by Carolin Schonard in eNeuro
Image: images/news/2022-11-11_rtl_news_schonard.jpg
Tags: reachandtouch
external_url: https://www.eneuro.org/content/9/6/ENEURO.0313-22.2022
---
