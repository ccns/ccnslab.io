---
Title: FWF grant for Andreas Wutz
Date: 29.12.2022
Summary: Dr. Andreas Wutz receives funding from the FWF for his proposal "Targeting large-scale neural dynamics in support of supramodal conscious perception".
Tags: ccns
---

Andreas Wutz has acquired funding by the FWF. 
The project will investigate neural correlates of consiousness (NCC) with the aim of identifying the large-scale neural patterns common to near threshold auditory and somatosensory perception. A neuroimaging guided neurostimulation approach will test whether putatively supramodal neural patterns are required for conscious visual perception. The project will combine different neuroimaging techniques with neurostimulation and will apply novel data analysis methods (representational similarity analysis) to localization of NCC in the brain. 
