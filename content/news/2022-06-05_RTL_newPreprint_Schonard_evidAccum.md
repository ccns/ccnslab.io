---
Title: Allocation of visuospatial attention indexes evidence accumulation... (preprint)
Date: 06-05-2022
Summary: New preprint by Carolin Schonard on bioRxiv
Image: images/news/2022-06-05_rtl_news_schonard.jpg
Tags: reachandtouch
external_url: https://www.biorxiv.org/content/10.1101/2022.05.06.490925v1
status: hidden
---

The deployment of attention is a prerequisite for for motor planning but its exact role remains unknown. By combining, behavioral experiments, psychophysics, and computational modeling, we show that the time-course of emergent, visuospatial attention reflects the time-extended, cumulative decision that leads to motor goal selection, offering a window onto the tight link of perceptual and motor aspects in sensorimotor decision-making.

Read preprint [here](https://www.biorxiv.org/content/10.1101/2022.05.06.490925v1)
