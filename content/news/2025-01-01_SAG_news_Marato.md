Title: Research project NEURO-FEM: Neuroimaging the Female Brain
Date: 01-01-2025
Summary: Neuroimaging the Female Brain: How ovarian hormones modulate brain dynamics along women’s lifespans. 
Image: 
Tags: ccns, sexandgender
external_url: 

Together with Anira Escrichs Martínez from the University Pompeu Fabra, the research group examines how ovarian hormones modulate brain dynamics along women’s lifespans.