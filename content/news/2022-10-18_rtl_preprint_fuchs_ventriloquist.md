---
Title: Modulation of implicitly perceived hand size by visuotactile recalibration
Date: 10-18-2022
Summary: New preprint by Xaver Fuchs on bioRxiv
Image: images/news/2022-10-18_rtl_preprint_fuchs.jpg
Tags: reachandtouch
external_url: https://www.biorxiv.org/content/10.1101/2022.10.13.512071v1
---

