---
Title: The hands’ default location guides tactile spatial selectivity
Date: 04-04-2023
Summary: New paper by Tobias Heed, together with collaborator Steph Badde (Tufts, Boston) in PNAS
Image: images/news/2023-04-04_rtl_paper_badde.jpg
Tags: reachandtouch
external_url: https://www.pnas.org/doi/abs/10.1073/pnas.2209680120
---
