---
Title: Effector selection precedes movement specification - evidence from repetition effects in motor planning
Date: 02-20-2025
Summary: New paper by Christian Seegelke and Tobias Heed
Image: images/news/2025-02-20_rtl_paper_seegelke.jpg
Tags: reachandtouch
external_url: https://link.springer.com/article/10.1007/s00221-025-07022-x
---
