---
Title: A Distributed Model of Face and Body Integration
Date: 08-11-2022
Summary: Review/Opinion paper by Celia Foster
Image: images/news/2022-08-11_rtl_news_foster.jpg
Tags: reachandtouch
external_url: https://doi.org/10.1177/26331055221119221
---
