Title: New article on long-term mental health
Date: 10-10-2024
Summary: The article discusses the importance of researching the mental health effects of hormonal contraceptives and the need to identify women at risk for adverse mood reactions before they start hormonal contraception. Read [here](https://doi.org/10.56367/OAG-044-11693)
Image:
Tags: ccns, sexandgender
external_url: https://doi.org/10.56367/OAG-044-11693
