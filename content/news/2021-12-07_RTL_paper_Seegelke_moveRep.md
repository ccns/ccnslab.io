---
Title: Repetition effects in action planning reflect effector- but not hemisphere-specific coding
Date: 12-07-2021
Summary: New paper by Christian Seegelke in Journal of Neurophysiology
Image: images/news/2021-12-07_rtl_news_seegelke_1170-665.jpg
Tags: reachandtouch
external_url: https://journals.physiology.org/doi/full/10.1152/jn.00326.2021
---

Repeated hand use facilitates the initiation of successive actions (repetition effect). By asking participants to perform successive actions with hands and feet we dissociated effector-independent, hemisphere-specific from effector-specific processing and provide novel evidence that repetition effects in limb use truly reflect effector-specific coding.

Read paper [here](https://journals.physiology.org/doi/full/10.1152/jn.00326.2021)
