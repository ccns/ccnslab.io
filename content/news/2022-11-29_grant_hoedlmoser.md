---
Title: FWF grant for Kerstin Hödlmoser
Date: 29.12.2022
Summary: Prof. Kerstin Hödlmoser receives funding from the FWF for her proposal on memory and sleep disturbances during pregnancy.
Tags: ccns
---

Kerstin Hödlmoser has acquired funding by the FWF. The funded project aims to better understand impairments of memory and sleep that are common during pregnancy. The project investigates whether memory deficits emerging during pregnancy are not only related to physical and hormonal changes, but also to changes in subjective (e.g., sleep quality and quantity) and objective (e.g., sleep architecture, sleep spindles, slow oscillations) sleep measures.
The project will be a longitudinal study that will assess memory performance in woman during and after preganancy, and controls over a period of 12 months. The data will be combined with state-of-the-art recordings of sleep and of hormonal change. The project will elucidate the relationship between these variables.