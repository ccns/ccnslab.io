---
Title: The Centre
Date: 01.06.2022
section: centre
---

The Centre for Cognitive Neuroscience (CCNS) aims to advance our understanding of the interplay between neural and cognitive processes. This interdisciplinary effort currently involves ~60 researchers from a variety of disciplines including psychology, biology, linguistics, sports and exercise science, law, neurology, and psychiatry. The close collaboration between the University and the Christian Doppler University Hospital facilitates the translational research needed to address today's pressing health and societal challenges.
