---
Title: Neuroimaging & Brain Stimulation
Date: 01.06.2022
Image: images/modalities/methods_mri_tms.jpg
---

We provide a 3 Tesla MRI scanner to record high resolution images of brain structure and blood flow changes related to neurons' activity during mental or motor tasks, during visual, auditory, and tactile stimulation, or when participants simply rest. Our setup can simultaneously record eye movements, heart rate, and other physiological parameters. We also provide transcranial magnetic stimulation (TMS) to safely manipulate cognitive processing through brain stimulation.
