---
Title: Electro&shy;encephalography (EEG) Unit
Date: 01.06.2022
Image: images/modalities/methods_eeg.jpg
---

We measure electrical brain, muscle and heart activity with very high time precision. These recordings are possible across all ages starting with newborns, as well as with patients including people who cannot communicate, such as post-comatose patients.
