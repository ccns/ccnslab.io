---
Title: Eye Tracking and Mental Chronometry Unit
Date: 01.06.2022
Image: images/modalities/methods_eye.jpg
---

This unit is a starting point for many studies that further probe cognition with the methods provided by the other units, such as EEG, MEG, fMRI, tACS/tDCS, and TMS. We can co-register eye movement and behavioral responses with fMRI and EEG, which allows us to exploit the strengths of the different methods all at once. Our facilities include eye movement measurement in VR, voice recording, and multi-subject testing.
