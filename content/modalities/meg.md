---
Title: Magneto&shy;encephalography (MEG) Unit
Date: 01.06.2022
Image: images/modalities/methods_meg.jpg
---

We employ a state-of-the-art whole head scanner that samples the brain magnetic fields with very high time resolution, optionally integrated with EEG.  Ancillary measurements include ECG, EMG, EOG, and binocular eye tracking.  Auditory, somatosensory,  and high-frequency visual stimulation devices complement the equipment, alongside with an MEG compatible transcranial electrical stimulation system.
