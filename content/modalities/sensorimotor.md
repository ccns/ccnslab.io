---
Title: Sensorimotor Stimulation & Tracking Unit
Date: 24.06.2022
Image: images/modalities/methods_sensmot.jpg
---

We record body movements to visual, auditory, and tactile stimulation with high time precision. A setup that can perturb and restrict hand movements allows testing cognitive influences on motor output via reflex-like responses and force measurement. Tracking and stimulation can be combined with EEG, TMS, eye tracking, and fMRI. 
