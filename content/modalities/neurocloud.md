---
Title: High Performance Computing Unit
Date: 30.08.2022
Image: images/modalities/methods_neurocloud.jpg
---

We aim to integrate Austria’s large-scale facilities in the field of cognitive neuroscience by establishing a centralized hub for standardised storage, management and analysis of research data. Modern data processing workflows and reproducible analysis pipelines will be integrated into a  high-performance computing environment optimized for data-intensive use cases. The Austrian NeuroCloud is committed to the EU Open Science policy and supporting the European Open Science Cloud.
