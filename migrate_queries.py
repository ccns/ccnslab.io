import frontmatter
import json
from pathlib import Path


class MyYamlHandler(frontmatter.default_handlers.YAMLHandler):
    def load(self, fm, **kwargs):
        parsed = super().load(fm, **kwargs)
        original_split = [x.split(':') for x in fm.split('\n')]
        if 'Affiliations' in parsed:
            affiliations_idx = [x[0].strip() for x in original_split].index('Affiliations')  # noqa
            affiliations = []
            for cur_entry in original_split[affiliations_idx + 1:]:
                if not cur_entry[0].startswith('  '):
                    break
                affiliations.append(cur_entry[0].strip())
            parsed['Affiliations'] = affiliations

        if 'Phone' in parsed:
            phone_idx = [x[0].strip() for x in original_split].index('Phone')
            parsed['Phone'] = original_split[phone_idx][1]

        return parsed

    def export(self, metadata: dict[str, object], **kwargs: object) -> str:
        rval = super().export(metadata, sort_keys=False, **kwargs)
        rval = rval.replace('- ', '    ')
        return rval

queries_file = Path('plugins/pure/queries.json')
all_members_md = list(Path('content').rglob('members/*.md'))

with queries_file.open() as f:
    queries = json.load(f)

new_queries = []

for cur_query in queries:
    if cur_query['entry_type'] != 'person':
        new_queries.append(cur_query)
        continue

    cur_md_file = [x for x in all_members_md if x.stem == cur_query['name']]
    if not cur_md_file:
        print(f"Could not find {cur_query['name']}")
        new_queries.append(cur_query)
        continue
    if len(cur_md_file) > 1:
        print(f"Multiple files found for {cur_query['name']}")
        new_queries.append(cur_query)
        continue

    cur_md_file = cur_md_file[0]

    parsed_file = frontmatter.load(cur_md_file, handler=MyYamlHandler())
    if cur_query['types'] == ['article'] and cur_query['size'] == 100:
        parsed_file.metadata['Pure'] = int(cur_query['pure_id'])
    else:
        parsed_file.metadata['Pure'] = {
            'id': int(cur_query['pure_id']),
            'types': cur_query['types'],
            'size': int(cur_query['size'])
        }

    frontmatter.dump(parsed_file, cur_md_file)

with queries_file.open('w') as f:
    json.dump(new_queries, f, indent=2, ensure_ascii=False)
