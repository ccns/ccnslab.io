from markdown.treeprocessors import Treeprocessor
from markdown.postprocessors import Postprocessor
from markdown.preprocessors import Preprocessor
from markdown.extensions import Extension


class AddNewLinesAroundJinja(Preprocessor):
    def run(self, lines):
        new_lines = []
        for idx, cur_line in enumerate(lines):
            is_jinja_tag = '{%' in cur_line or '{{' in cur_line

            if is_jinja_tag and (not new_lines or new_lines[-1] != ''):
                new_lines.append('')
            new_lines.append(cur_line)
            if is_jinja_tag and (len(lines) == (idx + 1) or
                                 lines[idx + 1] != ''):
                new_lines.append('')
        return new_lines


class RemoveParagraphsAroundJinja(Treeprocessor):
    def run(self, root):
        if root is not None:
            for cur_p in root.iter('p'):
                if cur_p.text is not None:
                    if '{%' in cur_p.text or '{{' in cur_p.text:
                        cur_p.tag = 'delete_me_later_jinja'


class RemoveFakeJinjaTags(Postprocessor):
    def run(self, text):
        return text.replace('<delete_me_later_jinja>', '')\
            .replace('</delete_me_later_jinja>', '')


class JinjaInMarkdownExtension(Extension):
    def extendMarkdown(self, md):
        md.preprocessors.register(AddNewLinesAroundJinja(),
                                  'add_newlines_jinja', 15)
        md.treeprocessors.register(RemoveParagraphsAroundJinja(),
                                   'remove_p_jinja', 15)
        md.postprocessors.register(RemoveFakeJinjaTags(),
                                   'remove_fake_jinja_tags', 15)


def makeExtension(**kwargs):
    return JinjaInMarkdownExtension(**kwargs)
