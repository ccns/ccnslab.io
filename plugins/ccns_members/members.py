from pelican import signals


def force_members_to_use_members_template(page_generator):
    member_pages = [x for x in page_generator.pages
                    if x.category.name == 'members']
    for cur_member in member_pages:
        cur_member.template = 'person'


def register():
    signals.page_generator_finalized.connect(
        force_members_to_use_members_template
    )
