import frontmatter
import requests
from pelican import signals
from pelican.contents import Article
import json
import datetime
import pickle
from pathlib import Path
import os
from thefuzz import fuzz


# A class representing Article meta information.
# Information is extracted from a complex API response.
class ArticleMeta:
    def __init__(self, raw_article_data):
        self.title = raw_article_data['title']['value']
        if 'subTitle' in raw_article_data:
            self.title += ': ' + raw_article_data['subTitle']['value']
        self.date = datetime.datetime.now()

        # Get the date of published status
        # (multiple statuses come in PURE result).
        for status in raw_article_data['publicationStatuses']:
            if status['current']:
                self.publication_year = status['publicationDate']['year']
                if 'month' in status['publicationDate']:
                    self.publication_month = datetime.datetime.strptime(
                        str(status['publicationDate']['month']),
                        "%m").strftime('%B')

        # Check for journal or conference contribution or whether it may be
        # a prepint.
        # For simplicity, the same meta field `journal_name`
        # is used to store it.
        if 'journalAssociation' in raw_article_data:
            self.journal_name = raw_article_data['journalAssociation']['journal']['name']['text'][0]['value']  # noqa
        elif 'hostPublicationTitle' in raw_article_data:
            self.journal_name = raw_article_data['hostPublicationTitle'][
                'value']
        # check whether it is a preprint. Probably more checks are needed
        # to distinguish it from other forms of pure entries !!!
        else:
            if 'publicationDate' in raw_article_data:
                self.journal_name = 'Preprint'

        self.authors = list()
        for a in raw_article_data['personAssociations']:
            if a:
                self.authors.append(
                    a['name']['firstName'] + ' ' + a['name']['lastName'])

        # Some research outputs have a `link` defined, not a `doi`.
        # If none of them is available, do not set `doi`.
        # Some research outputs have no `electronicVersions`.
        if 'electronicVersions' in raw_article_data:
            if 'doi' in raw_article_data['electronicVersions'][0]:
                self.doi = raw_article_data['electronicVersions'][0]['doi']
            elif 'link' in raw_article_data['electronicVersions'][0]:
                self.doi = raw_article_data['electronicVersions'][0]['link']

    # Returns a dict representation of article meta information
    # to be added later into Pelican's article.
    def to_dict(self):
        d = dict()
        d['title'] = self.title
        d['date'] = self.date
        d['publication_year'] = self.publication_year
        if hasattr(self, 'publication_month'):
            d['publication_month'] = self.publication_month
        if hasattr(self, 'journal_name'):
            d['journal_name'] = self.journal_name
        else:
            d['journal_name'] = "Preprint"
        d['authors'] = ', '.join(self.authors)
        d['author_list'] = self.authors
        if hasattr(self, 'doi'):
            d['doi'] = self.doi
        return d


# Builds a PURE query dict based on single query
# parameters defined in `queries.json`.
def build_pure_query(pure_id, entry_type, types=('article', ), size=100):
    pure_query = dict()

    # Constants
    pure_query['offset'] = 0
    pure_query['orderings'] = ['-publicationDate']
    pure_query['fields'] = [
        'title.value',
        'subTitle.value',
        'journalAssociation.journal.*',
        'hostPublicationTitle.value',
        'personAssociations.name.firstName',
        'personAssociations.name.lastName',
        'electronicVersions.doi',
        'electronicVersions.link',
        'publicationStatuses.current',
        'publicationStatuses.publicationDate.year',
        'publicationStatuses.publicationDate.month',
        'publicationStatuses.publicationStatus.uri',
    ]
    pure_query['publicationStatuses'] = [
        '/dk/atira/pure/researchoutput/status/published',
        '/dk/atira/pure/researchoutput/status/inpress',
        '/dk/atira/pure/researchoutput/status/epub'
    ]
    pure_query['publishedBeforeDate'] = (
        datetime.date.today() + datetime.timedelta(days=1)).strftime(
            '%Y-%m-%d')  # Tomorrow.

    # From parameters
    pure_query['size'] = size
    pure_query['typeUris'] = list()
    for type in types:
        if type == 'article':
            pure_query['typeUris'].append('/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/article')  # noqa
        if type == 'preprint':
            pure_query['typeUris'].append('/dk/atira/pure/researchoutput/researchoutputtypes/workingpaper/preprint')  # noqa
        if type == 'conference':
            pure_query['typeUris'].append('/dk/atira/pure/researchoutput/researchoutputtypes/contributiontobookanthology/conference')  # noqa
        if type == 'shortreport':
            pure_query['typeUris'].append('/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/shortsurvey')  # noqa
        if type == 'review':
            pure_query['typeUris'].append('/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/systematicreview')  # noqa
        if type == 'comment':
            pure_query['typeUris'].append('/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/comment') # noqa    
    if entry_type == 'person':
        pure_query['forPersons'] = dict()
        pure_query['forPersons']['ids'] = [pure_id]
    elif entry_type == 'unit':
        pure_query['forOrganisationalUnits'] = dict()
        pure_query['forOrganisationalUnits']['ids'] = [pure_id]
    else:
        raise ValueError(f'Unknown entry type {entry_type}')

    return pure_query


# Fetches research outputs from PURE using a query dict.
def get_research_outputs(pure_api_key, query):
    api_url = 'https://research.uni-salzburg.at/ws/api/524/research-outputs'
    header = {
        'Accept': 'application/json',
        'api-key': pure_api_key,
        'Content-Type': 'application/json'
    }
    research_outputs = []
    # `json.dumps` is used here because `query` is a dict and the default
    # string representation has single quotes.
    response = requests.post(api_url, headers=header, data=json.dumps(query))
    research_outputs = response.json()['items']
    return research_outputs


# Adds a list of research outputs as results of multiple API queries
# into Pelican's context.
def add_research_outputs_to_context(article_generator):
    article_generator.context['research_outputs'] = dict()
    PURE_API_KEY = os.getenv('PURE_API_KEY')
    if PURE_API_KEY:
        article_generator.context['research_outputs'] = research_output_to_dict(  # noqa
            PURE_API_KEY
        )
    elif os.getenv('$CI_COMMIT_BRANCH') == 'main':
        raise RuntimeError('No PURE_API_KEY was set although we are on the '
                           'main branch')
    else:
        print('No PURE_API_KEY specified. Using dumped version')
        with open(Path('misc_data', 'pure_data.dat'), 'rb') as pure_file:
            article_generator.context['research_outputs'] = pickle.load(
                pure_file
            )


def research_output_to_dict(pure_api_key):
    research_output = dict()

    with open('plugins/pure/queries.json', 'rb') as queries_file:
        queries = json.load(queries_file)

    all_members_md = Path('content').rglob('members/*.md')
    for cur_member_md in all_members_md:
        cur_frontmatter = frontmatter.load(cur_member_md)
        if 'Pure' in cur_frontmatter.metadata:
            this_query = {
                'name': cur_member_md.stem,
                'pure_id': '',
                'entry_type': 'person',
                'types': ['article'],
                'size': 100
            }

            if isinstance(cur_frontmatter['Pure'], int):
                this_query['pure_id'] = cur_frontmatter['Pure']
            elif isinstance(cur_frontmatter['Pure'], dict):
                cur_pure_data = cur_frontmatter['Pure']
                cur_pure_data['pure_id'] = cur_pure_data.pop('id')
                this_query.update(cur_pure_data)
            else:
                raise ValueError('Unknown Pure type')

            queries.append(this_query)

    for query_params in queries:
        pure_query = build_pure_query(
            pure_id=query_params['pure_id'],
            entry_type=query_params['entry_type'],
            types=query_params['types'],
            size=query_params['size']
        )
        raw_research_outputs = get_research_outputs(pure_api_key,
                                                    pure_query)
        raw_articles = []

        for p in raw_research_outputs:
            am = ArticleMeta(p)
            article = Article(
                content='',  # empty article's content
                metadata=am.to_dict()
            )

            raw_articles.append(article)

        raw_articles.sort(
            key=lambda x: datetime.date(year=x.metadata['publication_year'],  # noqa
                                        month=datetime.datetime.strptime(
                                            x.metadata.get('publication_month', 'January'), '%B').month,  # noqa
                                        day=1),
            reverse=True
        )

        titles = []
        articles = []
        for cur_article in raw_articles:
            sim_score = []
            if cur_article.title in titles:
                pass
            for check_dup in titles:
                sim = fuzz.ratio(check_dup, cur_article.title)
                if sim < 90:
                    sim_score.append(False)
                else:
                    sim_score.append(True)
            if any(sim_score):
                pass
            else:
                articles.append(cur_article)
                titles.append(cur_article.title)

        research_output[query_params['name']] = articles

    return research_output


def register():
    signals.article_generator_pretaxonomy.connect(
        add_research_outputs_to_context
    )
