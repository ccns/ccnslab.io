from pelican import signals
from pathlib import Path
from jinja2 import ChoiceLoader, Environment, FileSystemLoader


def evaluate_jinja(page_generator, content):
    local_dirs = page_generator.settings.get("MARKDOWN_TEMPLATES", ["."])
    local_dirs = [
        Path(page_generator.settings["PATH"], folder) for folder in local_dirs
    ]
    theme_dir = Path(page_generator.settings["THEME"], "templates")
    md_macros_dir = Path(theme_dir, 'md_macros')
    tags_dir = Path(theme_dir, 'tags')

    loaders = [FileSystemLoader(_dir)
               for _dir in local_dirs + [md_macros_dir, tags_dir, theme_dir]]
    if "JINJA_ENVIRONMENT" in page_generator.settings:  # pelican 3.7
        jinja_environment = page_generator.settings["JINJA_ENVIRONMENT"]
    else:
        jinja_environment = {
            "trim_blocks": True,
            "lstrip_blocks": True,
            "extensions": page_generator.settings["JINJA_EXTENSIONS"],
        }
    env = Environment(loader=ChoiceLoader(loaders), **jinja_environment)
    if "JINJA_FILTERS" in page_generator.settings:
        env.filters.update(page_generator.settings["JINJA_FILTERS"])
    if "JINJA_GLOBALS" in page_generator.settings:
        env.globals.update(page_generator.settings["JINJA_GLOBALS"])
    if "JINJA_TEST" in page_generator.settings:
        env.tests.update(page_generator.settings["JINJA_TESTS"])

    this_context = page_generator.context.copy()
    this_context['current_content'] = content

    content._content = env.from_string(content._content).render(
        this_context)


def register():
    signals.page_generator_write_page.connect(evaluate_jinja)
    signals.article_generator_write_article.connect(evaluate_jinja)
