from pelican import signals

# Indexes pages and builds a page hierarchy.
#
# Each page receives additional attributes:
# - `parent` page
# - list of `children` pages
# - if a page belongs to a unit:
#    - `unit_page` (landing unit page)


def assign_unit_to_children(page, unit_page):
    page.unit_page = unit_page
    for child in page.children:
        child.unit_page = unit_page
        assign_unit_to_children(child, unit_page)


def add_hierarchy(page_generator):
    unit_pages = []  # cache unit pages here
    for page in page_generator.pages:
        # Set parent attribute if not set
        if not hasattr(page, 'parent'):
            page.parent = None

        # get direct children of this page
        page.children = [x for x in page_generator.pages
                         if x.relative_dir == page.relative_source_path[:-3]]

        # Assign the current page as the parent of all direct children
        for p in page.children:
            p.parent = page

        if page.category.name == 'labs' or page.category.name == 'clusters':
            unit_pages.append(page)

    # Now take all the unit pages we found and assign itself as the unit
    # for all its children with recursion

    for page in unit_pages:
        assign_unit_to_children(page, page)


def register():
    signals.page_generator_finalized.connect(add_hierarchy)
