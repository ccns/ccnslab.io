# ccns.gitlab.io

The website of the Centre for Cognitive Neuroscience (CCNS): https://ccns.gitlab.io

[[_TOC_]]

## License

The content of this website is the subject to [CC BY license](https://creativecommons.org/licenses/by/4.0/). The source code is under [MIT license](https://opensource.org/licenses/MIT).

## Design

The website is implemented using [Pelican](https://docs.getpelican.com/en/latest/index.html) static site generator with the Markdown extension allowing the content be written using Markdown.

The graphical design is powered by [Bulma](https://bulma.io/). At the moment we use the defaults with [several customizations](themes/ccns/static/sass/ccns.scss) (responsive CSS-only menu, accordion). The CSS is then compiled with [Sass](https://sass-lang.com/).

## Content

`content` directory holds the website content.

### Pages

The pages are located in `content/pages/`. They are organized in a directory structure. Top-level pages are ccns pages and are represented in the main page navigation.

Each page has a header with the following meta fields.

| Meta field | Is mandatory | Description |
| :--------- | :----------- | :---------- |
| `Title`    | yes          | The title of a page used for the page title and name of menu item. |
| `Order`    | yes          | Used for sorting sibling pages in menus. Sorting is done with [Jinja sort filter](https://jinja.palletsprojects.com/en/3.1.x/templates/#jinja-filters.sort). If not defined there is no error and the behavior is undefined. |
| `short_title` | no | A short title of a page. Currently used for rendering shortened names of units in the navigation drop-down. See [`sexandgender` header](content/pages/labs/sexandgender.md) for an example. |
| `Template` | no           | Name of the template to use for page generation. If not defined, the [default page template](themes/ccns/templates/page.html) is used. |
| `temporary_url`| no | Temporary attribute reserved for top-level unit pages. It is used in the transition phase when a unit still has an external website. As the result, the link in the "CCNS" drop-down redirects to the external website. |

Cluster pages use additionally `Summary` and `Image` fields, similarly to news, to generate cluster cards on the front page.

Other meta fields can be defined for specific templates, for example for unit landing pages.

### Units

The website supports subpages with their own context for research *units*. In a unit context, additional unit navigation is generated. Currently there are two types of units: *labs* and *research clusters* (short *clusters*). We use the term *unit* whenever the distinction is not relevant. 

Each unit has a dedicated pages subdirectory. Each unit page is represented in the unit navigation.

| Unit type | Directory |
| :-------- | :-------- |
| lab | `content/pages/labs/` |
| cluster | `content/pages/clusters/` |

For content generation and filtering, each unit is assigned a key. The current keys mapping are:

| Unit key | Unit type | Unit name |
| :------- | :-------- | :-------- |
| `reachandtouch` | lab | Reach & Touch Lab |
| `neurocog` | lab | Neurocognition Lab |
| `sexandgender` | lab | Sex Hormones and Gender Neuroscience |
| `auditory` | lab | Auditory Neuroscience Lab |
| `eat` | lab | Eating Behavior Lab |
| `sleep` | lab | Sleep and Consciousness Lab |
| `braindynamics` | cluster | Brain Dynamics |
| `cognitive` | cluster | Cognitive Neuroscience and Brain Disorders |
| `development` | cluster | Developmental Aspects of Cognition |
| `fitness` | cluster | Mental and Physical Fitness |
| `hormones` | cluster | Hormones and the Brain |
| `law` | cluster | Neuroscience and the Law |
| `sleepcluster` | cluster | Sleep, Cognition and Consciousness |

A unit has custom menu which is generated from the hierarchy of unit pages. Menu rendering supports two levels: unit top-level pages and their children.

A unit can have a `themes/ccns/static/images/logo_<unit key>.png` logo file. This file is rendered right of CCNS logo in the navbar and acts as unit home link.

### People

Each person is described in a dedicated article file under [`./content/members/`](./content/members). The file name has the form `<name>_<surname>.md`, where `<name>` and `<surname>` are lower case, and umlauts are not allowed (for example, use `oe` for `ö`). For examples, look at the current files.

#### Metadata

The person file must have a header with the following fields.

| Meta field | Is mandatory | Description |
| :--------- | :----------- | :---------- |
| `Title` | yes | Person's full name (`Name Surname`). |
| `Date` | yes | Is required for articles, can be the date of creating the file. |
| `Name` | yes | First name. |
| `Surname` | yes | Surname. |
| `Tags` | no | A list of tags for filtering persons articles; use `ccns` tag to be displayed on the list of all members. |
| `Position` | no | Held position, for example `Full Professor`. |
| `Email` | no | Correctly formatted email address. |
| `Phone` | no | Phone number with the format `+43XXXXXXXXXXX` (no spaces and `+` in front). |
| `home_url` | no | Url, starting with `http`, of the home institution's page. For external persons, who are not members of CCNS, it is mandatory. If not defined for such persons, they won't have any link. |
| `Photo` | no | Photo file name of the form `<name>_<surname>.jpg` stored in `./content/images/members/`. `<name>` and `<surname>` are lower case, and umlauts are not allowed. If this field is not defined, a default photo is rendered. |
| `Affiliations` | no | A list of person's affiliations, each in separate line. There is no strict convention. However, the affiliation names should be consistent between persons. |
| `Pure` | no | Information necessary for the research outputs retrieved from PURE, including `id`, `types`, and `size`. More details about adding this information can be found below in the  [section on research outputs](#lists-of-research-outputs) |
| `CV` | no | Path of your cv file, provided as a single pdf file that can be downloaded via a link provided on your page. The file needs to be put in `content/assets/cv` and is called `YOURFIRSTNAME_YOURLASTNAME.pdf`|


#### Person template

Each person file generates a person website under the url `./members/<name>-<surname>.html`. The website is generated using the template [`./themes/ccns/templates/person.html`](./themes/ccns/templates/person.html). The same template is set automatically for every person under `EXTRA_PATH_METADATA` in the configuration files.

#### Generate a list of persons

List of persons can be generated by including [`themes/ccns/templates/members.html`](themes/ccns/templates/members.html) template into a higher-level template as in the following example. The [`members.html`](themes/ccns/templates/members.html) template splits the list of persons into rows of three and renders each person.

```jinja
{% set members = articles | filter_articles(category='members', tags=['ccns']) %}
{% include "members.html" %}
```
- `articles` holds all articles in the context. Each person is an article.
- `members` is the variable holding all persons to be rendered.
- [`filter_articles`](jinjafilters.py) is a custom Jinja filter for advanced article filtering. It can filter articles of specific `category` (persons are in [`members`](content/members/) category), having specific `tags`, or not having specific `excluded` tags. See the documentation in [`filter_articles`](jinjafilters.py) for more details.

For full and more complex examples, see [`themes/ccns/templates/members.ccns.html`](themes/ccns/templates/members.ccns.html) and [`themes/ccns/templates/members.reachandtouch.html`](themes/ccns/templates/members.reachandtouch.html).

### News

News articles are in `content/news` directory. Each news item is a separate article file. News are filtered for the main ccns page and the unit landing pages using article tags.

A news article file must have a header with the following fields.

| Meta field | Is mandatory | Description |
| :--------- | :----------- | :---------- |
| `Title` | yes | Person's full name (`Name Surname`). |
| `Date` | yes | Is required for articles, can be the date of creating the file. |
| `Summary` | yes | A brief news description that is rendered with the title on the list news lists. |
| `Image` | no | Path to an image to be displayed with the news. The news images are stored in `./content/images/news/`. There is no file name guideline at the moment. The path is `images/news/<your_image_file>`. |
| `external_url` | no | Url to external news origin, starting with `http`. If not defined, the news title in news list links to internal news page under `news/{slug}.html`. |
| `Tags` | no | A list of tags for filtering news articles; use `ccns` tag to be displayed on the CCNS front page. Use [unit key](#units) for filtering on unit pages. |

#### Naming News files

News filenames are formed YYYY-MM-DD_Unit_Abbreviation_topic_name.md
- Using the date first allows sorting and curating news files.
- Unit key, e.g. `rtl` for Reach & Touch Lab, allows sorting and curating news files.
- topic: e.g. preprint, paper, grant, prize...
- name: of the person about whom the news is, preferably last name

#### Generate a list of news articles

List of news articles can be generated by including one of two templates, one that will display images above the news if available, and one that will always ignore all images: [`themes/ccns/templates/news_with_pics.html`](themes/ccns/templates/news_with_pics.html), [`themes/ccns/templates/news_without_pics.html`](themes/ccns/templates/news_without_pics.html). The template must be included into a higher-level template as in the following example. The news templates split the list of news articles into rows of three and render each news. The number of news items to display can be set by slicing the generated list, see second line in jinja code below. 6 news items (i.e., 2 rows) can be obtained by slicing the `news` list with `[:6]`. See the [explanation of list slicing](https://stackoverflow.com/questions/509211/understanding-slicing) for more details.

```jinja
{% set news = articles | filter_articles(category='news', tags=['ccns']) %}
{% set news = news[:6] %}
{% include "news_with_pics.html" %}
```
- `articles` holds all articles in the context. Each news is an article.
- `news` is the variable holding all news articles to be rendered.
- [`filter_articles`](jinjafilters.py) is a custom Jinja filter for advanced article filtering. It can filter articles of specific `category` (news are in [`news`](content/news/) category), having specific `tags`, or not having specific `excluded` tags. See the documentation in [`filter_articles`](jinjafilters.py) for more details.

For full example, see [`themes/ccns/templates/index.reachandtouch.html`](themes/ccns/templates/index.reachandtouch.html).

### Projects

Often a unit works on a number of projects. The projects articles can be put in `content/projects` directory. Then, they can be filtered and rendered similarly to news. This documentation should be extended once the projects find more use cases.

## Markdown guidelines

### Messages

[Messages](https://bulma.io/documentation/components/message/) can be rendered with the `{% message %}` tag using the following syntax.

```
{% message %}
My message.
{% endmessage %}
```

Messages can have a header with a `title`, and support [`color`](https://bulma.io/documentation/components/message/#colors) and [`size`](https://bulma.io/documentation/components/message/#sizes) attributes.

```
{% message title="My message title", color="is-info", size="is-medium" %}
My message.
{% endmessage %}
```

The body of the message is treated as Markdown.

### Links

[Linking to internal content](https://docs.getpelican.com/en/stable/content.html#linking-to-internal-content) (articles or pages) should be done with the Pelican's `{filename}` directive. This ensures the correct URL generation in every environment. Examples:

```Markdown
[a link to a page relative to lab's front page]({filename}sexandgender/research/projects.md)
[a link to an article relative to the content root directory]({filename}/news/some_news_article.md)
```

### Images with text

By default, images included in markdown are rendered on the website with their original resolution or downsized to the text width. The text before and after the image is rendered above and below the image, respectively. This behavior is not optimal, especially for wider screens.

Our custom `image_with_text` tag renders images next to the accompanying text. Try it in your markdown file:
```
{% image_with_text image_file="images/labs/reachandtouch/methods/MRT.png", image_caption="Image: Reach and touch lab", image_side="right", text_title="fMRT", hide_image_mobile=False %}
Functional magnetic resonance imaging (fMRI) is a non-invasive imaging method to measure blood flow changes in the brain. Researchers assume that blood level oxygenation is  a proxy for neuronal processing, allowing us to characterize which processes brain regions are involved in, their connectivity to other regions, and what kind of coding a region uses with high spatial resolution across the course of an experimental task.
{% endimage_with_text %}
```

You can view an example on https://ccns.plus.ac.at/labs/reachandtouch/research/methods/

The text between `{% image_with_text [parameters] %}` and `{% endimage_with_text %}` is rendered next to the image.

The following `[parameters]` are available. The optional parameters may be left out.

| Parameter | Description | Is optional | Default value |
| :-------- | :---------- | :---------- | :------------ |
| `image_file` | The path to the image file, starting with `images/`. Images are stored in `./contents/images/` and further organized similarly to markdown files with the content. | No | - |
| `image_caption` | The image caption text rendered under the image. | No | - |
| `image_side` | The side of the text to render the image, `"left"` or `"right"`. | No | `"left"` |
| `text_title` | The title of the text, rendered above the text with larger and thicker font. | Yes | `None` |
| `hide_image_mobile` | Hide the image in mobile view (narrow screen). | Yes | `False` |

#### Alternating image side

You can use this tag several times in a row. Then, alternating the image side may be more visually appealing. Then, however, in the mobile view (resize the browser window to try it out) the images and texts don't alternate. There are two solutions: don't alternate the image sides, or use `hide_image_mobile=True` parameter to hide images in the mobile view.

#### Image orientation, width, and resolution

The image orientation (horizontal or vertical) is recognized automatically. This is necessary to configure the rendered image width. Vertical images should be narrower than horizontal. Otherwise, they occupy too much space.

The images are resized to specific width. You may want to provide images with high enough resolutions, such the the rendered image is not larger than its original resolution.

#### Long texts

The image and text are enclosed in two columns, each occupying half of the page. The image and text are centered vertically. If the text is long, it may occupy too much space compared to the image. This won't be visually appealing. Therefore, the text length should be adjusted to the image height.

## Lists of research outputs

The `plugins/pure` plugin generates lists of publications by using the [Elsevier PURE](https://uni-salzburg.elsevierpure.com/) API. We currently use a POST request to [`/research-outputs` endpoint](https://test-research.sbg.ac.at/ws/api/522/api-docs/index.html#!/research45outputs/listResearchOutputs_1). Each research output in the result is converted to an internal representation as [`ArticleMeta`](plugins/pure/pure.py#L10). `ArticleMeta` is further converted to Pelican's [`Article`](plugins/pure/pure.py#L110). The plugin [injects the generated list of `Article`s into the Pelican's context as a dict variable](plugins/pure/pure.py#L116-L118) `research_outputs`.

An example of research outputs is the list of [nine most recent CCNS journal publications](https://ccns.gitlab.io/#recent-publications).

Currently, the plugin supports six types of research outputs. 

| research output |key in `queries.json` | PURE type |
|:----------------|:---------------------|:----------|
| journal article | `"article"` | `/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/article` |
| Conference contribution | `"conference"` | `/dk/atira/pure/researchoutput/researchoutputtypes/contributiontobookanthology/conference` 
| Preprint | `"preprint"` | `/dk/atira/pure/researchoutput/researchoutputtypes/workingpaper/preprint`
| Systematic Review | `"review"` | `/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/systematicreview` |
| Short Report | `"shortreport"` | `/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/shortsurvey` |
| Comment | `"comment"` | `/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/comment` |


To create your list of publications on your members page on the CCNS site using the plugin, add the information needed for the PURE query to your personal member `.md` file. The file is usually found within the `members` folder of your lab, usually following the scheme of `content/pages/labs/YOURLABNAME/members/YOURFIRSTNAME_YOURLASTNAME.md`.
Information about the person `.md` files is given above in the section [People](#people).

In the header of your person `.md` file, add the information relevant for the query:

```
Pure:
   id: YOUR_PURE_ID
   types:
         article
         preprint
         shortreport
         review
         comment
   size: 100`
```

`"id"` defines your personal PURE id, a number with 8 digits, that you can retrieve within your PURE account. 
`"types"` defines the types of research outputs that you would like to appear on your personal page. If you, for example, would like to only include journal articles, put "article" and omit the remaining types.  
`"size"` limits the number of research outputs. `"types"` is a list of one or more research output types.

The default values for `type` is "article" and for `size` it is "100". So if you are fine with the defaults, you can just add:

```Pure: YOUR_PURE_ID```

The generated list of `Article`s can be then accessed in the templates with `research_outputs['myqueryname']`.

### Lists of research outputs for non-person pages

For some pages, for example for the whole CCNS, there is no person `.md` file. For these cases, the entries need to be put within a `queries.json` file. 
Proceed as follows:

Add your query to [`plugins/pure/queries.json`](`plugins/pure/queries.json`) with the name `myqueryname`. The query is a JSON object and has the following format. Be careful because there is no validation at the moment.
   ```json
   {
        "name": "myqyeryname",
        "pure_id": "12345678",
        "entry_type": "person",
        "size": 100,
        "types": ["article", "conference", "preprint", "shortreport", "review", "comment"]
   }
   ```
   

## Structure

The `plugins/ccns-hierarchy` plugin automatically generates the ccns pages hierarchy. For the moment, see comments in `plugins/ccns-hierarchy/hierarchy.py`.

## Template

`themes/ccns` holds the Pelican template files and custom CSS.

## Deployment

We use GitLab CI/CD to build and publish the website with GitLab Pages. For development we use merge requests which cause slightly different pipeline.

To review the development version, push the changes to a merge request and access the generated website through View lates app button in the merge request.

## Develop in your web browser
If you just want to add or modify some content or do minor editing, you do not have to install
anything on your computer. You can do everything in the web browser.

In order to do this, scroll to the top of this page. You will find a part there that looks like this:

![gitlab header with Web IDE selected](assets/gitlab_web_ide.png)

Notice the button that says "Web IDE".

**Do not click it yet!!**

Instead, click on the small downward pointing arrow to its right and choose "Gitpod".
Now, it should look like this:

![gitlab header with Gitpod selected](assets/gitlab_gitpod.png)

**Click the button now!!**

The first time you click it, you need to give gitpod permission to access
your gitlab account. Do that. Afterwards, it is going to open a Visual
Studio Code environment in your browser. It automatically sets up everything
for you and starts a development server. So, you can see all the changes directly in
your browser!

**IMPORTANT**

Saving your modified files **is not enough!!** Three minutes after you close the
browser tab or 30 minutes after doing anything, the session gets deleted.

**In order to save your work, you need to commit and push it!!**

Gitpod gives you 50 hours per month for free. This should be enough for normal content edits.

## Building locally
If you want to do more extensive edits, you can also create a local developers
environment.

There are two main types of dependencies: Python dependencies and CSS (and maybe in the future) 
Javascript dependencies. Python dependencies are handled using `pip` while CSS/JS dependencies are
handled using `npm`.

Additionally, the css has to be compiled.

All commands are executed from the project root.

### Install Python and its dependencies

1. Make sure you have python installed. Either use the [excellent Anaconda Distribution](https://www.anaconda.com/products/distribution),
   install it with your package manager if you use Linux or [download it directly from the Python website](https://www.python.org/downloads/).
2. Create an environment for the python packages.
   1. If you use anaconda, you do this:
      ```bash
      conda create -p ./.venv python
      conda activate ./.venv
      ```
   2. If you choose a different way of installing python you can use [Python virtual environment](https://docs.python.org/3/tutorial/venv.html):
      ```bash
      python -m venv .venv
      source .venv/bin/activate
      ```
3. Install the necessary packages:
   ```bash
   pip install -U -r requirements.txt
   ```

### Install npm and the CSS dependencies

CSS and possible future javascript dependencies are handled by `npm` which 
is built on nodejs.

1. Make sure you have a recent **LTS** version of nodejs and npm. You can either
   install it [using a package manager](https://nodejs.org/en/download/package-manager/)
   (recommended solution for Linux and Mac and probably also for windows) or
   [download an installer from their website](https://nodejs.org/en/download/)
   and install it.
2. Use `npm` to install all the packages:
   ```bash
   npm install
   ```
   
### Compile the CSS

This website uses [bulma](https://bulma.io) as a CSS framework. To compile it
including the customizations, run:

```bash
npm run build_css
```

### Run the developer version locally

You can run the developer version of this website locally by running:

```bash
npm run dev_server
```

Now browse to http://localhost:8000 to see the website. If you change a source
file, the website updates automatically.

Please keep in mind that this will use a cached version of the publication list.
So it does not reflect the current status in PURE.

#### How to update the cached publication list

If you want to update the publication list for the local development, you
need to run:

```bash
PURE_API_KEY="<your-token>" python create_pure_dump.py
```

## Debugging

### Pelican error

To debug an **error reported by Pelican**, add `--debug` flag to Pelican's command. It will then print a stack trace to the standard output.

### Jinja template

To debug a **template**, it is sometimes useful to print the variables available from the template's context. To do so, use the Debug Extension of Jinja.

Add the following to `pelicanconf.py`:
```python
JINJA_ENVIRONMENT = {
    'extensions': ['jinja2.ext.debug']
}
```
and add the debug tag to the template:
```html
<pre>{% debug %}</pre>
```
You will see on your website a list of available variables.
