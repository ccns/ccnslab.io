import sys
sys.path.append('.')

from baseconf import *  # noqa

SITEURL = 'https://ccns.plus.ac.at/'
RELATIVE_URLS = False
PAGE_URL = '{path_no_ext}/'
