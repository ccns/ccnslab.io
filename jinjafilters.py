def filter_articles(input, category=None, tags=None, exclude=None):
    """Filters out articles in a list.

    Returns articles from `input` with a specified category and a set of tags.
    Each returned article must have all tags from `tags` and none from
    `exclude`. In order for the filters to work, an article must have the
    filtering attributes defined.

    The `tags` and `exclude` lists are converted internally to sets. Jinja
    doesn't seem to support sets.

    Args:
        input: A list of articles to filter.
        category: A category to which articles must belong.
        tags: A list of tags which an article must have.
        exclude: A list of tags which an article must not have.

    Returns:
        A list of articles after filtering.
    """
    result = []

    filter_category = category is not None
    filter_tags = False
    if tags is not None:
        filter_tags = True
        tags = set(tags)
    filter_exclude = False
    if exclude is not None:
        filter_exclude = True
        exclude = set(exclude)

    for article in input:
        has_category = True
        if filter_category:
            has_category = False
            # Article must be in `category`.
            if hasattr(article, 'category') and article.category == category:
                has_category = True

        article_tag_set = set()
        if hasattr(article, 'tags'):
            article_tag_set = set(article.tags)

        has_tags = True
        if filter_tags:
            has_tags = False
            # Article must have all `tags`.
            if article_tag_set and tags.issubset(article_tag_set):
                has_tags = True

        has_no_exclude = True
        if filter_exclude:
            has_no_exclude = False
            # Article must not have any tags from `exclude`.
            if article_tag_set and not bool(article_tag_set & exclude):
                has_no_exclude = True

        if has_category and has_tags and has_no_exclude:
            result.append(article)

    return result
