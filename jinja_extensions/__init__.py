from .publications import PublicationParagraphExtension, PublicationCardsExtension
from .components import MessageExtension
from .inclusiontag import InclusionTag
from .funding import FundingExtension
from .design_elements import ImageWithTextExtension

def get_extensions():
    return [PublicationParagraphExtension, MessageExtension, PublicationCardsExtension, FundingExtension, ImageWithTextExtension]

