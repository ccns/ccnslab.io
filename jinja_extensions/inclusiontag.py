from jinja2_simple_tags import StandaloneTag, ContainerTag


class InclusionMixin():
    use_template = None

    def get_context(self, *args, **kwargs):
        raise NotImplementedError()

    def get_template(self):
        if self.use_template is not None:
            return self.use_template

        raise RuntimeError('You must either set '
                           'the use_template property or '
                           'implement the get_template method.')


class InclusionTag(StandaloneTag, InclusionMixin):
    def render(self, *args, **kwargs):
        ctx = self.get_context(*args, **kwargs)
        template = self.environment.get_template(self.get_template())
        return template.render(**ctx)


class ContainerInclusionTag(ContainerTag, InclusionMixin):
    def render(self, *args, caller=None, **kwargs):
        ctx = self.get_context(*args, caller=caller(), **kwargs)
        ctx['caller'] = caller()
        template = self.environment.get_template(self.get_template())
        return template.render(**ctx)
