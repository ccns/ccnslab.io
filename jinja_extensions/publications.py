from jinja2_simple_tags import StandaloneTag
from metapub import CrossRefFetcher
from types import SimpleNamespace
import dateutil

from .inclusiontag import InclusionTag


class PublicationParagraphExtension(StandaloneTag):
    tags = {'publication_paragraph'}

    def render(self, publication_or_doi, bold_author_names=[]):
        if isinstance(publication_or_doi, str):
            cf = CrossRefFetcher()
            article_metadata = cf.article_by_doi(publication_or_doi)

            article = {
                'doi': f'https://doi.org/{article_metadata.doi}',
                'title': article_metadata.title[0],
                'author_list': article_metadata.author_list,
                'journal_name': article_metadata.container_title[0]
            }

            publication = SimpleNamespace(**article)
        else:
            publication = publication_or_doi

        if hasattr(publication, 'doi'):
            pub_main = f'<a href="{publication.doi}"><strong>{publication.title}</strong></a>'  # noqa
        else:
            pub_main = f'<strong>{publication.title}</strong>'

        author_list = []
        for author in publication.author_list:
            if author in bold_author_names:
                this_author = f'<strong>{author}</strong>'
            else:
                this_author = author
            author_list.append(this_author)

        authors = ', '.join(author_list)
        return f'''
<div class="block">
    <dl>
        <dt>
            {pub_main}
        </dt>
        <dd>{authors}</dd>
        <dd class="is-italic">{publication.journal_name}</dd>
    </dl>
</div>'''


class PublicationCardsExtension(InclusionTag):
    tags = {'publication_cards'}
    use_template = 'tags/publication_cards.html'

    def get_context(self, names=None, tags=None,
                    batch_size=3, max_articles=None):
        if names is not None and tags is not None:
            raise ValueError('You can only set the names '
                             'or tags paramater, not both!')
        if tags is not None:
            all_members = [x for x in self.context['pages']
                           if x.category == 'members']
            this_members = [x for x in all_members if set(x.tags) & set(tags)]
            names = [f'{x.name}_{x.surname}'.lower() for x in this_members]

        all_publications = []
        for cur_name in names:
            try:
                all_publications.extend(
                    self.context['research_outputs'][cur_name]
                )
            except KeyError:
                pass

        unique_publications = []
        for pub in all_publications:
            if pub.slug not in [x.slug for x in unique_publications]:
                unique_publications.append(pub)

        unique_publications.sort(
            key=lambda x: self._article_sort_key(x), reverse=True
        )

        if max_articles:
            unique_publications = unique_publications[:max_articles]

        return {
            'publications': unique_publications,
            'batch_size': batch_size
        }

    def _article_sort_key(self, article):
        if hasattr(article, 'publication_month'):
            date_str = (f'{article.publication_month}, '
                        f'{article.publication_year}')
        else:
            date_str = str(article.publication_year)

        key = int(dateutil.parser.parse(date_str).timestamp())
        return key
