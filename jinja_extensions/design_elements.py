from .inclusiontag import ContainerInclusionTag
from PIL import Image


class ImageWithTextExtension(ContainerInclusionTag):
    tags = {'image_with_text'}
    use_template = 'tags/image_with_text.html'

    # The image width in the template varies for different orientations.
    # This function returns the image orientation.
    def get_image_orientation(self, image_path):
        with Image.open('content/' + image_path) as img:
            width, height = img.size
            if width > height:
                return "horizontal"
            else:
                return "vertical"

    def get_context(self, image_file, image_caption, image_side='left',
                    text_title=None, hide_image_mobile=False, caller=None):
        return {
            'image_file': image_file,
            'image_caption': image_caption,
            'image_side': image_side,
            'image_orientation': self.get_image_orientation(image_file),
            'text_title': text_title,
            'hide_image_mobile': hide_image_mobile,
            'text': caller
        }
