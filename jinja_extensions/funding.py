from .inclusiontag import InclusionTag


class FundingExtension(InclusionTag):
    tags = {'funding'}
    use_template = 'tags/funding.html'

    def get_context(self, funding_dict, root_folder):
        return {
            'funding': funding_dict,
            'root_folder': root_folder
        }
