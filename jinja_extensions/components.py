from jinja2_simple_tags import ContainerTag


class MessageExtension(ContainerTag):
    tags = {'message'}

    def render(self, title=None, size='is-normal', color='is-dark',
               caller=None):
        header_template = '''
            <div class="message-header">
                <p>{title}</p>
            </div>
        '''
        message_template = '''
            <article class="message {size} {color}">
            {message_header}
            <div class="message-body">
                {message_body}
            </div>
            </article>
        '''

        message_header = ''
        if title:
            message_header = header_template.format(title=title)
        return message_template.format(
            size=size,
            color=color,
            message_header=message_header,
            message_body=caller()
        )
